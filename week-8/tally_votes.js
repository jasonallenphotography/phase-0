// Tally Votes in JavaScript Pairing Challenge.

// I worked on this challenge with: Hanna Taylor
// This challenge took us [6] hours.

// These are the votes cast by each student. Do not alter these objects here.
var votes = {
  "Alex": { president: "Bob", vicePresident: "Devin", secretary: "Gail", treasurer: "Kerry" },
  "Bob": { president: "Mary", vicePresident: "Hermann", secretary: "Fred", treasurer: "Ivy" },
  "Cindy": { president: "Cindy", vicePresident: "Hermann", secretary: "Bob", treasurer: "Bob" },
  "Devin": { president: "Louise", vicePresident: "John", secretary: "Bob", treasurer: "Fred" },
  "Ernest": { president: "Fred", vicePresident: "Hermann", secretary: "Fred", treasurer: "Ivy" },
  "Fred": { president: "Louise", vicePresident: "Alex", secretary: "Ivy", treasurer: "Ivy" },
  "Gail": { president: "Fred", vicePresident: "Alex", secretary: "Ivy", treasurer: "Bob" },
  "Hermann": { president: "Ivy", vicePresident: "Kerry", secretary: "Fred", treasurer: "Ivy" },
  "Ivy": { president: "Louise", vicePresident: "Hermann", secretary: "Fred", treasurer: "Gail" },
  "John": { president: "Louise", vicePresident: "Hermann", secretary: "Fred", treasurer: "Kerry" },
  "Kerry": { president: "Fred", vicePresident: "Mary", secretary: "Fred", treasurer: "Ivy" },
  "Louise": { president: "Nate", vicePresident: "Alex", secretary: "Mary", treasurer: "Ivy" },
  "Mary": { president: "Louise", vicePresident: "Oscar", secretary: "Nate", treasurer: "Ivy" },
  "Nate": { president: "Oscar", vicePresident: "Hermann", secretary: "Fred", treasurer: "Tracy" },
  "Oscar": { president: "Paulina", vicePresident: "Nate", secretary: "Fred", treasurer: "Ivy" },
  "Paulina": { president: "Louise", vicePresident: "Bob", secretary: "Devin", treasurer: "Ivy" },
  "Quintin": { president: "Fred", vicePresident: "Hermann", secretary: "Fred", treasurer: "Bob" },
  "Romanda": { president: "Louise", vicePresident: "Steve", secretary: "Fred", treasurer: "Ivy" },
  "Steve": { president: "Tracy", vicePresident: "Kerry", secretary: "Oscar", treasurer: "Xavier" },
  "Tracy": { president: "Louise", vicePresident: "Hermann", secretary: "Fred", treasurer: "Ivy" },
  "Ullyses": { president: "Louise", vicePresident: "Hermann", secretary: "Ivy", treasurer: "Bob" },
  "Valorie": { president: "Wesley", vicePresident: "Bob", secretary: "Alex", treasurer: "Ivy" },
  "Wesley": { president: "Bob", vicePresident: "Yvonne", secretary: "Valorie", treasurer: "Ivy" },
  "Xavier": { president: "Steve", vicePresident: "Hermann", secretary: "Fred", treasurer: "Ivy" },
  "Yvonne": { president: "Bob", vicePresident: "Zane", secretary: "Fred", treasurer: "Hermann" },
  "Zane": { president: "Louise", vicePresident: "Hermann", secretary: "Fred", treasurer: "Mary" }
}

// Tally the votes in voteCount.
var voteCount = {
  president: {},
  vicePresident: {},
  secretary: {},
  treasurer: {}
}

/* The name of each student receiving a vote for an office should become a property
of the respective office in voteCount.  After Alex's votes have been tallied,
voteCount would be ...

  var voteCount = {
    president: { Bob: 1 },
    vicePresident: { Devin: 1 },
    secretary: { Gail: 1 },
    treasurer: { Kerry: 1 }
  }

*/


/* Once the votes have been tallied, assign each officer position the name of the
student who received the most votes. */
var officers = {
  president: undefined,
  vicePresident: undefined,
  secretary: undefined,
  treasurer: undefined
}

// Pseudocode
/*
Input: votes object
Output: winner of each position in voteCount (which tallies the votes)
Steps:
1) Iterate over the votes object to evaluate ballots.
  - Count the number of votes each ballot has for each position.
    - IF a value isn't present in voteCount.[position], push it to the object with a value of 1.
    - ELSE IF it does exist, increment the value in voteCount.[position] plus 1.
2) Iterate over the voteCount object to determine if there is a winner
  - Evaluate the values of each position
  - IF a key has the largest value, push that position's value to officers.[position]


*/
// __________________________________________
// Initial Solution
/*
voteCount.president
votes[voterBallot].president

  voteCount = {
  president: votes[ballot].president.push(),
  vicePresident: votes[ballot].vicePresident,
  secretary: votes[ballot].secretary,
  treasurer: votes[ballot].treasurer}

votes[ballot].president

function tally(votes){
for (var voterBallot in votes) {
  for (var position in voteCount) {
    if (voteCount[position].hasOwnProperty(votes[voterBallot][position])) {
      voteCount[position][votes[voterBallot][position]] += 1;
    }
    else {
      voteCount[position][votes[voterBallot][position]] = 1;
      }
    }
  }
}
tally(votes);

// BROKEN winner function
// BROKEN winner function
// BROKEN winner function

We tried to"
  1. FOR loop over `election`
  2. create a holder array that receives the value of each position's candidate's vote totals
  3. sort the holder array to determine the highest vote total
  4. if the value of highestVote equals any candidate's vote total, push them to the Officers object

  Ultimately we could not find a solution to step 4 and abandoned this idea altogether. Through research we found a way to

function winner(election) {
  for (var position in election){
      console.log(position);
      var holder = []
      var results = election[position]

        for (var candidate in election[position]){
         var candidateVotes = election[position][candidate]
          holder.push(candidateVotes);
        }

    var highestVote = holder.sort(function(a, b){return b-a})[0];
    //Magic happens
    //If a candidate in election[position] has highestValue votes, push them to officers[position]
    }
  }
};


//WORKING winner function
//WORKING winner function
//WORKING winner function

function winners(election){
  for (var position in election){
    var x = election[position];
    var y = 0
    var winner

    for (var name in x){
         if ((x[name] > y)) {
           y = x[name];
           winner = name
         }
    officers[position] = winner
    }
  }
}

winners(voteCount);

*/
// __________________________________________
// Refactored Solution

// Refactored variable names in tally function
//  and winners function for readability

function tally(votes){
for (var voterBallot in votes) {
  for (var position in voteCount) {
    var officeResults = voteCount[position]
    var ballotPosition = votes[voterBallot][position]
    if (officeResults.hasOwnProperty(ballotPosition)) {
      officeResults[ballotPosition] += 1;
    }
    else {
      officeResults[ballotPosition] = 1;
      }
    }
  }
}
tally(votes);

function winners(election){
  for (var position in election){
    var office = election[position];
    var highest = 0
    var winner

    for (var name in office){
         if ((office[name] > highest)) {
           highest = office[name];
           winner = name
         }
    officers[position] = winner
    }
  }
}

winners(voteCount);

// __________________________________________
// Reflection
/*
What did you learn about iterating over nested objects in JavaScript?

  I learned that pseudocode becomes very important when looping over
  nested objects in JS. It's easy to quickly lose track of what key
  value a loop needs to be targeting in order to get the desired result.
  Using descriptive variable names helps in that process, and ultimately
  led to a successful initial solution for my pair and I on this exercise.

Were you able to find useful methods to help you with this?

  One part we had difficulty with was determining which position key had
  the highest value in our winners function. We were able to use a
  placeholder variable `highest`, an IF statement to find the highest
  value, and then an assignment to set the winner.

What concepts were solidified in the process of working through this challenge?

  Looping within a loop is one concept that we have a greater understanding of.
  Working with nested data sets is something that we also worked a great deal with.
  Pseudocode and refactoring for descriptive variable names led to
  much more readable code also.

*/
// __________________________________________
// Test Code:  Do not alter code below this line.


function assert(test, message, test_number) {
  if (!test) {
    console.log(test_number + "false");
    throw "ERROR: " + message;
  }
  console.log(test_number + "true");
  return true;
}

assert(
  (voteCount.president["Bob"] === 3),
  "Bob should receive three votes for President.",
  "1. "
)

assert(
  (voteCount.vicePresident["Bob"] === 2),
  "Bob should receive two votes for Vice President.",
  "2. "
)

assert(
  (voteCount.secretary["Bob"] === 2),
  "Bob should receive two votes for Secretary.",
  "3. "
)

assert(
  (voteCount.treasurer["Bob"] === 4),
  "Bob should receive four votes for Treasurer.",
  "4. "
)

assert(
  (officers.president === "Louise"),
  "Louise should be elected President.",
  "5. "
)

assert(
  (officers.vicePresident === "Hermann"),
  "Hermann should be elected Vice President.",
  "6. "
)

assert(
  (officers.secretary === "Fred"),
  "Fred should be elected Secretary.",
  "7. "
)

assert(
  (officers.treasurer === "Ivy"),
  "Ivy should be elected Treasurer.",
  "8. "
)
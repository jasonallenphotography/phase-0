# DBC Exercise 8.4 solutions
## Release 4: Schema Design

## Release 5: SELECTing Correctly
1. Select all data for all states.

  `SELECT * FROM states;`

2. Select all data for all regions.

  `SELECT * FROM regions;`

3. Select the state_name and population for all states.

  `SELECT state_name,population
  FROM states;`

4. Select the state_name and population for all states ordered by population.
  The state with the highest population should be at the top.

  `SELECT state_name,population
  FROM states
  ORDER BY population DESC;`

5. Select the state_name for the states in region 7.

  `SELECT state_name
  FROM states
  WHERE region_id = 7;`

6. Select the state_name and population_density for states with a population
  density over 50 ordered from least to most dense.

  `SELECT state_name,population_density
  FROM states
  WHERE population_density > 50
  ORDER BY population_density;`

7. Select the state_name for states with a population between 1 million and 1.5 million people.

  `SELECT state_name
  FROM states
  WHERE population
  BETWEEN 1000000 AND 1500000;`

8. Select the state_name and region_id for states ordered by region in ascending order.

  `SELECT state_name,region_id
  FROM states
  ORDER BY region_id;`

9. Select the region_name for the regions with "Central" in the name.

  `SELECT region_name
  FROM regions
  WHERE id
  BETWEEN 4 AND 7;`

10. Select the region_name and the state_name for all states and regions in ascending order by region_id. Refer to the region by name. (This will involve joining the tables).

  `SELECT regions.region_name,states.state_name
  FROM states
  INNER JOIN regions
  ON states.region_id=regions.id
  ORDER BY regions.id;`

## Release 6: Your Own Schema
![My Schema](my_schema.png "My SQL design schema.")


##Release 7: Reflect
###What are databases for?

Databases are structures for storing, searching, recalling and parsing information.


###What is a one-to-many relationship?

A one-to-many relationship is best described through the example of a car owner who owns many cars - owner ID holds constant for a set of cars, but there is one person with that owner ID.


###What is a primary key? What is a foreign key? How can you determine which is which?

A primary key is a unique key that identifies the instance of a database. A foreign key is a key from outside the table that corresponds to values in the table. Primary keys can't be null, though foreign keys can. There can be only one primary key in a table, though a table may contain many foreign keys.


###How can you select information out of a SQL database? What are some general guidelines for that?

One can select information by the column it is contained in, but the unique identifier in the instance of data in the table, by a range examining a column's value in a table, and many other different ways, as SQL databases are very flexible.

Some general guidelines for data retrieval are to be specific, return as small of a result as possible for best performance, optimize database structures so that results aren't parented through long chains, query for values equaling an intended result or range as inequals operators will return tremendously more results, avoid using wildcard statements at the start of a query as it will again return tremendous amounts of results and require many system resources.
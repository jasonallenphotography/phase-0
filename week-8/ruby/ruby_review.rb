# I worked on this challenge by myself.
# This challenge took me [2.5] hours. (Will update as I complete more review exercises)


# Pseudocode
# Input: An array of numbers
# Output: An array of numbers and strings
# Steps:
# Define a method "super_fizzbuzz" that accepts an argument
# IF a number is evenly divisible by 5 AND by 3, replace the number with the string "Fizzbuzz"
# ELSE IF a number is evenly divisible by 3, replace the number with the string "Fizz"
# ELSE IF a number is evenly divisible by 5, replace the number with the string "Buzz"
# ELSE leave the number as-is
# Print the array's elements as the loop evaluates and acts on the elements


# Initial Solution

# def super_fizzbuzz(array)
# array.each do |x|
#     if x % 3 == 0 && x % 5 == 0
#       x = "Fizzbuzz"
#     elsif x % 3 == 0
#       x = "Fizz"
#     elsif x % 5 == 0
#       x = "Buzz"
#     else
#       x = x
#     end
#     puts x
#   end
# end


# Refactored Solution
# variables represent modulo conditions for readability
# cases instead of if/elsif/else for readability

def super_fizzbuzz(array)
  array.each do |x|
    mod3 = x % 3 == 0
    mod5 = x % 5 == 0
    case
      when (mod3 && mod5) then puts "Fizzbuzz"
      when mod3 then puts "Fizz"
      when mod5 then puts "Buzz"
      else puts x
    end
  end
end

# nums = [1,2,3,4,5,15]
# super_fizzbuzz(nums)
class Dude
  attr_writer :thing
  attr_reader :thing

  def initialize(name)
    new_bro = Bro.new(name)
  end
end

class Bro

  def initialize(name)
    @name = name
  end
end

new_dude = Dude.new("Ty")

# FIBONACCI
# FIBONACCI
# FIBONACCI

# I worked on this challenge [by myself, with: ].
# This challenge took me [#] hours.

# Pseudocode
# Logical solution for determining if a number is Fibonacci from a math wiki entry:
# A number is Fibonacci if and only if one or both of (5*n^2 + 4) or (5*n^2 – 4) is a perfect square
#
# INPUT: integer
# OUTPUT: boolean true if fibonacci, or false if not
# STEPS:
  # Define a method is_fibonacci? that accepts an integer argument
  # Define a variable 'a' that contains the value of (5*n2 + 4)
  # Define a variable 'b' that contains the value of (5*n2 - 4)
  # IF 'a' is a perfect square OR 'b' is a perfect square, return true

# Initial Solution


# def is_fibonacci?(num)
#   a = Math.sqrt(5 * (num ** 2) + 4 ) #variable 'a' holding (5*n2 + 4)
#   b = Math.sqrt(5 * (num ** 2) - 4 ) #variable 'b' holding (5*n2 - 4)
#   p ( a == a.round || b == b.round )
# end

# The above code worked for smaller fibonacci numbers, but flunked RSpec test for
# a large number that exceeded Ruby's floating point (lack of) precision

# p is_fibonacci?(2) # Should/does return true
# p is_fibonacci?(5) # Should/does return true
# p is_fibonacci?(13) # Should/does return true
# p is_fibonacci?(30) # Should/does return false

# Refactored Solution
# REFACTORED STEPS for passing RSpec, based on research on StackOverflow:
  # Require the Ruby method BigDecimal
  # Define a method is_fibonacci? that accepts an integer argument
  # Define a variable 'i' that is a new BigDecimal
  # Define a variable 'a' that contains the value of (5*n2 + 4)
  # Define a variable 'b' that contains the value of (5*n2 - 4)
  # IF 'a' is a perfect square OR 'b' is a perfect square, return true

require 'bigdecimal'

def is_fibonacci?(num)
  num = BigDecimal.new(num)
  a = (5 * (num ** 2) + 4 ).sqrt(0) #variable 'a' holding square root of (5*n2 + 4)
  b = (5 * (num ** 2) - 4 ).sqrt(0) #variable 'b' holding square root of (5*n2 - 4)
  p ( a == a.round || b == b.round )
end

is_fibonacci?(43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875)

# Reflection
=begin

if num*2 *5 + 4 || ... -4 == perfect square ? true

What concepts did you review or learn in this challenge?

In FizzBuzz I reviewed control flow, refactoring, cases, and logical problem solving.

    In the Fibonacci challenge, I spent a good amount of time logical problem solving
    and pseudocoding before even touching the code, but once coding, I learned more
    about the limitations of Ruby's handling of large numbers, and how to work around
    those limitations with Ruby gems.


What is still confusing to you about Ruby?

    I'm finding that I'm becoming quite comfortable with Ruby, but I want to learn more
    about what is happening "under the hood", and what limitations of accuracy there are
    when calling some of the built in functions that I take for granted, like the Math.sqrt
    function, which had unexpected performance implications when handling very large numbers
    and evaluating them against an algorithm.


What are you going to study to get more prepared for Phase 1?

    I plan on further reviewing the book "The Well Grounded Rubyist", as well as another
    book "Practical Object Oriented Programming in Ruby" that I acquired, as well as
    "The Ruby Cookbook" - which I've found helpful for refactoring. I'm also reading
    through the Jon Duckett "JS and JQuery" book, and independently
    learning more about JS.

=end


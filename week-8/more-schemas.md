## Release 2: Create a One-to-one Schema

The screenshot below is indicative of a one-to-one schema relationship. While a person may have many forms of ID, they will have only one driver's license.

Their primary key could be their Social Security Number (pk_ssn on the table 'people' below), and the foreign key field containing their SSN would be in the related table (fk_ssn on the table 'drivers_license' below) with their driver's license number, vehicle operator class, their corresponding insurance company and policy number contained externally.

![Release 2](imgs/one-to-one.png "Release 2")


## Release 4: Refactor

For my grocery list database scheme below, my join table is 'orders', which accepts 'grocery_lists' primary key as a foreign key in 'from_list', and accepts 'items' primary key 'item_sku' into the foreign key 'has_items'. 'grocery_lists' also accepts 'items_sku' into 'grocery_lists.items'.

![Release 4](imgs/grocery-list.png "Release 4")


## Release 6: Reflect

### What is a one-to-one database?

A one-to-one database is a case where the primary key in one data table refers to a unique foreign key in another data table. They should be associated with each other based on only that one matching row.


### When would you use a one-to-one database? (Think generally, not in terms of the example you created).

A good use of a one-to-one database would be linking a table of people to a table of passport information for a specific country (which would be a single instance unique to that person).


### What is a many-to-many database?

A many to many database structure relates multiple data tables to one another through a join table with multiple foreign keys.


### When would you use a many-to-many database? (Think generally, not in terms of the example you created).

Managing tables of customers, items, and orders would be one use of a many-to-many structure, as well as managing tables of company business units' employees (i.e. a table for Accounting, Procurement, Management, etc), office locations, and payroll.

### What is confusing about database schemas? What makes sense?

Database schemas make a lot of sense to me and seem straightforward, particularly when visually represented. I'm curious to learn more about advantages/disadvantages related to performance on various more complex database structures.
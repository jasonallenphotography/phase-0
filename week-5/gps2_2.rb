# Release 1: Pseudocode
# Method to create a list
# input: string of items separated by spaces (example: "carrots apples cereal pizza")
# steps:
  # split the string into an array, using spaces as the divider.
  # create a new hash and loop over the array, setting the array elements to keys in the hash with a default value of 1. If an item appears more than once, increment the value by 1.
  # print the list to the console (i.e. call the print method)
# output: the hash, with the items as keys and the quantities as values

# Method to add an item to a list
# input: item name and optional quantity (default = 1)
# steps:
#   Check if the item is already in the hash.
#   If so, print "Item already on list"
#   If not, create a new key-value pair with the item as the key and the quantity as the value
# output: updated hash

# Method to remove an item from the list
# input: a string and optional quantity (default = 1)
# steps:
#  Check if the item is already in the hash.
#  If so, remove the key and corresponding value from hash.
#  If not, print "Item is not in hash"

# Method to update the quantity of an item
# input: string and quantity (positive or negative)
# steps:
#  Check if the item is in the hash.
#   If so, check if the existing value plus the quantity parameter is greater than or equal to zero.
#     If so, add the quantity parameter to the value for that item.
#   If less than or equal to zero, prompt to check if user wants to remove item.
#     If so, remove key-value pair and print list
#     If not, print "List will remain unchanged" and return to menu
# output: updated hash

# Method to print a list and make it look pretty
# input: command to print
# steps:
#  print 'Here is your grocery list (did you remember milk?)'
#  loop over the hash and print, on a new line:
#    [key] : [value]
# output: series of strings

# Note: we'll also create a menu that allows the user to access these methods
#  input: a string
#  steps:
#  print a welcome message and list of accepted prompts
#  prompt user
#  if the user input is a listed prompt, call the corresponding method
#  else present an error 'I didn't understand that' and re-prompt
#  output: a method call or an error

# REFLECTION
# REFLECTION
# REFLECTION

# What did you learn about pseudocode from working on this challenge?
# => Pseudocode definitely helps provide a useful reference once actual coding begins on a larger project with many moving pieces. You can revisit the pseudocode to remind yourself of the purpose of a method or class if you get too far down into the weeds

# What are the tradeoffs of using Arrays and Hashes for this challenge?
# => Hashes were necessary for this assignment because of the key/value pairing. We didn't find any limitations to Hashes. Arrays could store the relevant information (and we used a user-input array to create the hash initially with default values of "1") but could not maintain a specific item's value. A Hash grocery list of 3 apples, 2 bananas is much tidier {apple=>3; banana=>2} whereas an array list would be difficult to look at [apple, apple, apple, banana, banana] and tedious to work with.

# What does a method return?
# => A method automatically returns its last line unless otherwise specified with a `return`.

# What kind of things can you pass into methods as arguments?
# => Any object can be passed in as an argument.

# How can you pass information between methods?
# => Global variables are accessible by the whole program and can be used as a container for the data to be manipulated and returned/printed by other methods and classes. Alternately, a class variable with methods being defined in that class can share information.

# What concepts were solidified in this challenge, and what concepts are still confusing?
# => I'm becoming more comfortable with iterating across arrays using .each, and setting up and using hashes, but I keep finding myself referencing a cheatsheet of enumerators, comparators, and syntax that I wish I were more familiar with!

# ORIGINAL FILE CREATED WITH PAIR FOLLOWS ON LINES 75-153

# def newlist(list)
# $groceries = Hash.new()
#   list_array = list.split(" ")
#   list_array.each do |item|
#   $groceries.store(item,1)
#     end
# end

# def add_item(item_name, quantity = 1)
#   if $groceries.include?(item_name)
#     puts "That's already on the list."
#   else
#     $groceries[item_name] = quantity
#   end
#   p $groceries
# end


# def update_item(item_name, quantity = 1)
#   if $groceries.include?(item_name)
#     $groceries[item_name] = quantity
#   else puts "That item isn't on the list."
#   end
#   p $groceries
# end

# def remove_item(item_name)
#   if $groceries.include?(item_name)
#     $groceries.delete(item_name)
#     puts "That item has been removed!"
#   else
#     puts "That item isn't on the list!"
#   end
#   p $groceries
# end

# def printlist
#   puts "Here is your grocery list (did you remember milk?)"
#   $groceries.each do |k,v|
#     puts k.capitalize + " : " + v.to_s
#   end
# end


# def menu
#   puts "Welcome to your grocery list!"
#   puts "Please select one of the following options:"
#   puts "1) Create a new list"
#   puts "2) Add an item to your list"
#   puts "3) Update an item on your list"
#   puts "4) Remove an item from your list"
#   puts "5) Print your list"
#   puts "6) Exit"
#   puts "Please enter the menu item number:"
#     menu_selection = gets.chomp

#   if menu_selection == "1"
#     puts "Please enter your list."
#     newlist(gets.chomp)
#   elsif menu_selection == "2"
#     puts "What item would you like to add to your list?"
#     add_item(gets.chomp)
#   elsif menu_selection == "3"
#     puts "What item would you like to update?"
#     update_item(gets.chomp)
#   elsif menu_selection == "4"
#     puts "What item would you like to remove?"
#     remove_item(gets.chomp)
#   elsif menu_selection == "5"
#     printlist
#   elsif menu_selection == "6"
#     exit
#   else
#     puts "I'm sorry, I didn't understand that command."
#   end
#   menu
# end

# menu

#REFACTORED CODE WITH UI/UX IMPROVEMENTS, AND MORE OF A PROMPT & RESPONSE INPUT FOLLOWS THROUGH THE END OF THE DOCUMENT

def newlist(list)
$groceries = Hash.new()
  list_array = list.split(" ")
  list_array.each do |item|
  $groceries.store(item,1)
    end
    printlist
end

def add_item
    puts ""
    puts "********************************************"
    puts "What item would you like to add?"
    puts "********************************************"
    puts ""
  item_name = gets.chomp
    puts ""
    puts "********************************************"
    puts "How many would you like to add?"
    puts "********************************************"
    puts ""
  quantity = gets.chomp

  if $groceries.include?(item_name)
    puts ""
    puts "********************************************"
    puts "That's already on the list."
    puts "********************************************"
    puts ""
    printlist
  else
    $groceries[item_name] = quantity
    printlist
  end
end


def update_item
    puts ""
    puts "********************************************"
    puts "What item would you like to Update?"
    puts "********************************************"
    puts ""
  item_name = gets.chomp
    puts ""
    puts "********************************************"
    puts "How many do you want to save to your list?"
    puts "********************************************"
    puts ""
  quantity = gets.chomp
  if $groceries.include?(item_name)
    $groceries[item_name] = quantity
  else
    puts ""
    puts "********************************************"
    puts "#{item_name.to_s.capitalize} isn't on the list."
    puts "********************************************"
    puts ""
  end
  printlist
end

def remove_item
    puts ""
    puts "********************************************"
    puts "Here is your grocery list!"
      $groceries.each do |k,v|
      puts k.capitalize + " : " + v.to_s
      end
    puts "What item would you like to remove?"
    puts "********************************************"
    puts ""
    item_name = gets.chomp
  if $groceries.include?(item_name)
    $groceries.delete(item_name)
    puts ""
    puts "********************************************"
    puts "#{item_name.to_s.capitalize} has been removed!"
    puts "********************************************"
    puts ""
    printlist
  else
    puts ""
    puts "********************************************"
    puts "#{item_name.to_s.capitalize} isn't on the list!"
    puts "********************************************"
    puts ""
  end
end

def printlist
    puts ""
    puts "********************************************"
    puts "Here is your grocery list!"
    $groceries.each do |k,v|
    puts k.capitalize + " : " + v.to_s
  end
    puts "********************************************"
    puts ""
end

def error
    puts ""
    puts "********************************************"
    puts "I'm sorry, I didn't understand that command."
    puts "********************************************"
    puts ""
end


def menu
  puts ""
  puts "Welcome to your grocery list!"
  puts "Please select one of the following options:"
  puts "1) Create a new list"
  puts "2) Add an item to your list"
  puts "3) Update an item on your list"
  puts "4) Remove an item from your list"
  puts "5) Print your list"
  puts "6) Exit"
  puts "Please enter the menu item number:"
    menu_selection = gets.chomp

  if menu_selection == "1"
    puts ""
    puts "********************************************"
    puts "Enter a few items to start a list."
    puts "Separate your items with a space."
    puts "********************************************"
    puts ""
    newlist(gets.chomp)
  elsif menu_selection == "2"
    add_item
  elsif menu_selection == "3"
    update_item
  elsif menu_selection == "4"
    remove_item
  elsif menu_selection == "5"
    printlist
  elsif menu_selection == "6"
    exit
  else
    error
  end
  menu
end

menu

# newlist('peas pizza')
# add_item('juices', 2)
# update_item('juices', 5)
# remove_item('juices')
# printlist


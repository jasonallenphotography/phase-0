# Release 1: Pseudocode
# Method to create a list
# input: string of items separated by spaces (example: "carrots apples cereal pizza")
# steps:
  # split the string into an array, using spaces as the divider.
  # create a new hash and loop over the array, setting the array elements to keys in the hash with a default value of 1. If an item appears more than once, increment the value by 1.
  # print the list to the console (i.e. call the print method)
# output: the hash, with the items as keys and the quantities as values

# Method to add an item to a list
# input: item name and optional quantity (default = 1)
# steps:
#   Check if the item is already in the hash.
#   If so, print "Item already on list"
#   If not, create a new key-value pair with the item as the key and the quantity as the value
# output: updated hash

# Method to remove an item from the list
# input: a string and optional quantity (default = 1)
# steps:
#  Check if the item is already in the hash.
#  If so, remove the key and corresponding value from hash.
#  If not, print "Item is not in hash"

# Method to update the quantity of an item
# input: string and quantity (positive or negative)
# steps:
#  Check if the item is in the hash.
#   If so, check if the existing value plus the quantity parameter is greater than or equal to zero.
#     If so, add the quantity parameter to the value for that item.
#   If less than or equal to zero, prompt to check if user wants to remove item.
#     If so, remove key-value pair and print list
#     If not, print "List will remain unchanged" and return to menu
# output: updated hash

# Method to print a list and make it look pretty
# input: command to print
# steps:
#  print 'Here is your grocery list (did you remember milk?)'
#  loop over the hash and print, on a new line:
#    [key] : [value]
# output: series of strings

# Note: we'll also create a menu that allows the user to access these methods
#  input: a string
#  steps:
#  print a welcome message and list of accepted prompts
#  prompt user
#  if the user input is a listed prompt, call the corresponding method
#  else present an error 'I didn't understand that' and re-prompt
#  output: a method call or an error




def newlist(list)
$groceries = Hash.new()
  list_array = list.split(" ")
  list_array.each do |item|
  $groceries.store(item,1)
    end
end

def add_item(item_name, quantity = 1)
  if $groceries.include?(item_name)
    puts "That's already on the list."
  else
    $groceries[item_name] = quantity
  end
  p $groceries
end


def update_item(item_name, quantity = 1)
  if $groceries.include?(item_name)
    $groceries[item_name] = quantity
  else puts "That item isn't on the list."
  end
  p $groceries
end

def remove_item(item_name)
  if $groceries.include?(item_name)
    $groceries.delete(item_name)
    puts "That item has been removed!"
  else
    puts "That item isn't on the list!"
  end
  p $groceries
end

def printlist
  puts "Here is your grocery list (did you remember milk?)"
  $groceries.each do |k,v|
    puts k.capitalize + " : " + v.to_s
  end
end


def menu
  puts "Welcome to your grocery list!"
  puts "Please select one of the following options:"
  puts "1) Create a new list"
  puts "2) Add an item to your list"
  puts "3) Update an item on your list"
  puts "4) Remove an item from your list"
  puts "5) Print your list"
  puts "6) Exit"
  puts "Please enter the menu item number:"
    menu_selection = gets.chomp

  if menu_selection == "1"
    puts "Please enter your list."
    newlist(gets.chomp)
  elsif menu_selection == "2"
    puts "What item would you like to add to your list?"
    add_item(gets.chomp)
  elsif menu_selection == "3"
    puts "What item would you like to update?"
    update_item(gets.chomp)
  elsif menu_selection == "4"
    puts "What item would you like to remove?"
    remove_item(gets.chomp)
  elsif menu_selection == "5"
    printlist
  elsif menu_selection == "6"
    exit
  else
    puts "I'm sorry, I didn't understand that command."
  end
  menu
end

menu

# newlist('peas pizza')
# add_item('juices', 2)
# update_item('juices', 5)
# remove_item('juices')
# printlist
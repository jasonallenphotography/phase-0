# Die Class 1: Numeric

# I worked on this challenge by myself

# I spent [2] hours on this challenge.

# 0. Pseudocode

# Input:
# => Positive, non-zero Integer argument equal to the number of sides of the die
# Output:
# => Result of the 'roll' of the die - random Integer between 1 and the input argument
# Steps:
# => Initialize receives an argument variable `sides`. Check that IF sides is greater than 1 or return an ArgumentError ELSE convert sides to an integer and END.
# => Method `sides` returns the value of how many sides the die has.
# => Method `roll` returns a random integer from a range of 1 up through and including the number of sides of the die.


# 1. Initial Solution

# class Die
#   def initialize(sides)
#     @sides = sides.to_i
#     if @sides < 1
#       raise ArgumentError.new("The die must have at least 1 side!")
#     else
#       p @sides
#     end
#   end

#   def sides
#     p @sides
#   end

#   def roll
#     result = rand(1..@sides)
#     p result
#   end
# end

# die = Die.new(6)
# die.sides
# die.roll
# die = Die.new(0)

# 3. Refactored Solution
# Removed the `p @sides` and `p result` that were put in for viewing the output as a test. Added a text string to make the result more readable. Removed the call for `die.sides` once it was working correctly and printing the class variable `@sides`. Removed the class method call `die = Die.new(0)` that was a test case to see if the ArgumentError would display correctly, which it did.

class Die
  def initialize(sides)
    @sides = sides.to_i
    if @sides < 1
      raise ArgumentError.new("The die must have at least 1 side!")
    else
      return
    end
  end

  def sides
    p @sides
  end

  def roll
    result = rand(1..@sides)
    puts "You rolled a #{@sides.to_s}-sided die, and the result was #{result.to_s}." # Puts "You rolled a 20-sided die, and the result was 11." for a D20.
    return result
  end
end

die = Die.new(20)
die.roll

# 4. Reflection
# What is an ArgumentError and why would you use one?
# => An ArgumentError is a definable error alerting a user to an issue with their input into a method's argument. You'd use an ArgumentError to tell the user exactly what they did wrong that disagreed with the program so that they can avoid it in the future!

# What new Ruby methods did you implement? What challenges and successes did you have in implementing them?
# => I used class variables for the first time, as well as ArgumentError.new(). I did not have any challenges or issues implementing them.

# What is a Ruby class?
# => A Ruby class is a grouping of objects that can share instance variables.

# Why would you use a Ruby class?
# => Defining a class lets a developer share instance variables across methods, and organizes complicated code.

# What is the difference between a local variable and an instance variable?
# => A local variable is only usable within a method's definition. A class or instance variable can be called by another object within a class.

# Where can an instance variable be used?
# => An instance variable is usable anywhere within the class that it's contained. For example, in my code above, on line 51, we're defining an instance variable. Anywhere after that point until the class definition `ends` on line 68, other methods can (and do on lines 60, 64, and 65). In order to carry an instance variable outside of the class, you would have to return it to the console and assign its value to a variable, i.e.`external_variable_object = die.sides`
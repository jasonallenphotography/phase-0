# Release 1 PSEUDOCODE
# Define a method that accepts a string `students` as an argument. Split this string based on commas into an array, randomize the order of the array, and progressively output groups of no less than 3 and no more than 5 members.

# Release 2 - INITIAL SOLUTION with added randomization
# def acct_groups(list)
#   students = Array.new(list.to_s.split(','))
    # students.sort_by (rand)
#     while students.length >= 20
#       4.times {puts students.shift}
#       puts "*****     *****     *****"
#     end
#     while students.length > 0
#       5.times {puts students.shift}
#       puts "*****     *****     *****"
#     end
#   p students
#  end

#Release 3: Add complexity (OPTIONAL)

# If you want to take your solution a step further, consider these questions and make changes to your solution based on your decisions:

# If you run this program three times in a row, will the program give you three different outputs?
# => YES! I randomized my solution.

# How do you decide when you're done? You'll need to decide. This is much more challenging than you probably think, especially since there are no pre-written tests.
# => I allotted about an hour and a half for this exercise.

#Release 4: Refactor
#I refactored my randomization method by changing line 7 above to a .shuffle call on line #32 below. I also added UI/UX formatting for display, and an incremental group counting mechanism that prints accountability group numbers to the formatted output.
def acct_groups(list)
  students = Array.new(list.to_s.split(',').shuffle)
     group_count = 1
    while students.length >= 20
      puts "*****     *****     *****"
      puts "Group Number #{group_count}"
      4.times {puts students.shift}
      puts "*****     *****     *****"
      group_count += 1
    end
    while students.length > 0
      puts "*****     *****     *****"
      puts "Group Number #{group_count}"
      5.times {puts students.shift}
      puts "*****     *****     *****"
      group_count += 1
    end
  p students
 end

 #Release 5: Driver code/method call using the DBC Rockdoves cohort class list!

 acct_groups("Adam Pinsky,Afaan Naqvi,Alana Farkas,Andrew Crowley,Andrew Vathanakamsang,Anna Lansfjord,Benjamin Heidebrink,Blair White,Brad Lindgren,Brian Donahue,Brittney Braxton,Camila Crawford,Charlie Lee,Chunyu Ou,Daniel Homer,Daniel Shapiro,David Kaiser,David Ramirez,Emily Osowski,Emmet Garber,Eric Gumerlock,Gregory Toprak,Hanna Taylor,Dave Hostios,Jack Baginski,Jaclyn Feminella,Jasmeet Chatrath,Jason Allen,Jon Norstrom,jonathan nicolas,Joseph Yoo,Joshua Lugo,Joshua Wu,Julia Giraldi,Kari Giberson,Katherine Broner,Kevin Fowle,Kevin Niu,Kevin Perkins,Leo Kukhar,Li-Lian Ku,Erica Lloyd,Marita Deery,Michael Verthein,Milorad Felbapov,Brian Mosley,Nick Russo,Noah Schutte,Oscar Delgadillo,Ryan Dempsey,Ryan Wilkins,Sami Zhang,Saralis Rivera,Sean Norton,Shyh Hwang,Sydney Rossman-Reich,Theo Paul,Tomasz Sok,Wesley El-Amin")

#Release 6: REFLECTION
# What was the most interesting and most difficult part of this challenge?
# => I did not find the exercise too challenging. The most interesting part was testing my output to make certain that the groups were being divided according to the rules presented: No more than 5, no less than 3.

# Do you feel you are improving in your ability to write pseudocode and break the problem down?
# => I'm getting more comfortable with the idea of Pseudocode to conceptualize an idea before 'getting my hands dirty' with the code, but I would like more practice and will do so on my own.

# Was your approach for automating this task a good solution? What could have made it even better?
# => This solution works for +/- 1 from our class count. There are a few cases where a remaindered group at the end could be 1 or 2. A broader remaindering approach could prevent that for all cases - I built this specific to our test case though.

# What data structure did you decide to store the accountability groups in and why?
# => I used Arrays for this exercise as there was no need to hold values for each name in my approach.

# What did you learn in the process of refactoring your initial solution? Did you learn any new Ruby methods?
# => I learned the .shuffle method for randomization! I have a feeling I'll be using that a lot from now on. Much simpler than array.sort_by (rand)
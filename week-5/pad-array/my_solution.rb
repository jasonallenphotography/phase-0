# Pad an Array

# I worked on this challenge with: Kevin Perkins

# I spent [1.5] hours on this challenge.


# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented in the file.



# 0. Pseudocode

# What is the input?
  # Our input is an Array of objects, a minimum size parameter for our output, and an optional argument named `value` which is `nil` by default that will be pushed to the input `array` when the method is called.

# What is the output? (i.e. What should the code return?)
  # The output is an array that has been padded by (min_size) minus (array's current size) additional position(s).

# # What are the steps needed to solve the problem?
#   1) define methods pad and pad!
#   2) compare array input and minimum size input
#   3) IF array's count is equal to or greater than min size, return original array
#   4) ELSE pad the array to reach minimum size (including optional padding argument)

# 1. Initial Solution
def pad!(array, min_size, value = nil) #destructive
  if array.count >= min_size
    p array
  else
    size_diff = min_size - array.count
    size_diff.times {|x| array.push(value)}
    p array
  end
end

#BROKEN `pad` method that flunked rspec
# def pad(array, min_size, value = nil) #non-destructive
#   new = Array.new(array)
#   if new.count >= min_size
#     p array
#   else
#     size_diff = min_size - new.count
#     size_diff.times {|x| new.push(value)}
#     p new
#     p array
#   end
# end


# 3. Refactored Solution

def pad(array, min_size, value = nil) #non-destructive
  new = Array.new(array)
  if new.count >= min_size
    p new
  else
    size_diff = min_size - new.count
    size_diff.times {|x| new.push(value)}
    p new
  end
end

# 4. Reflection
# Were you successful in breaking the problem down into small steps?
    # Yes, my pair and I had great communication throughout the exercise and were able to pseudocode this into a simple process.

# Once you had written your pseudocode, were you able to easily translate it into code? What difficulties and successes did you have?
    #For our destructive `pad!()` method, yes we came to a quick and simple solution. We had a few hangups on our initial nondestructive solution.

# Was your initial solution successful at passing the tests? If so, why do you think that is? If not, what were the errors you encountered and what did you do to resolve them?
    # Our `pad!()` method passed our rspec tests right away.

    # The `pad()` method had a few hiccups. If no padding was required, we were returning the variable `array`. While the output VALUE was correct, our rspec was complaining that our result had the same object ID as the input variable. This was solved by defining `new` as a new array equal to the input variable `array`.

    # Further, in testing our program, I added `p array` to the else section to visually compare our result with the input, which gave the rspec a series of failures as the last returned result was expected to be the padded output, and it was seeing the input. That caused a host of cascading failures that were solved by simply removing `p array` so the console only saw `p new`

# When you refactored, did you find any existing methods in Ruby to clean up your code?
    #No, but our code is pretty concise to begin with. I'd be eager to hear insight into refactoring it further!

# How readable is your solution? Did you and your pair choose descriptive variable names?
    # Our naming scheme is pretty transparent. The variable `new` defines our new array object, `size-diff` defines the size difference between our input array's length and our output array's defined minimum length.

# What is the difference between destructive and non-destructive methods in your own words?
    # Destructive methods change the value of the input variable. Non-destructive methods leave the original variable untouched, and perform the work externally on a new object.
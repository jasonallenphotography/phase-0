# Numbers to Commas Solo Challenge

# I spent [2.5] hours on this challenge.

# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented in the file.

# 0. Pseudocode

# What is the input?
# => The input is an integer argument.

# What is the output? (i.e. What should the code return?)
# => The output is a string containing the integer argument, separated by commas in United States standard formatting.

# What are the steps needed to solve the problem?
# => The input is converted to an Integer
# => If Integer < 1000, return Integer as a string
# =>  ELSIF Integer > 1000 AND Integer < 999999, insert a comma between the hundreds and thousands place, and RETURN Integer as a string
# =>  ELSE Integer > 1000000 AND Integer < 999999999, Insert a comma between the hundreds and thousands place, and between the millions and hundred thousands place, and RETURN Integer as a string
# =>  ENDIF

# 1. Initial Solution
=begin

def separate_comma(integer)
  if integer.to_i < 1000
    p integer.to_s
  elsif
    integer > 1000 && integer < 999999
    integer = integer.to_s.insert(-4,",")
    p integer.to_s
  else
    integer > 1000000 && integer < 999999999
    integer = integer.to_s.insert(-4,",")
    integer = integer.to_s.insert(-8,",")
    p integer.to_s
  end
end

=end

# 2. Refactored Solution
# The limitations of my first approach to `separate_comma` is that it only covers instances up to and including 999,999,999, which while it covers the rspec for this exercise, doesn't grow for all cases. From values of 1 billion onwards the program's output is incorrect. Further the code is long and cumbersome, and doesn't fit DRY. Here's a refactored approach that should work correctly for all positive integers, and satisfy the rspec requirements.

def separate_comma(integer)
  if integer.to_s.length < 4
    p integer.to_s
  else integer.to_s.length >= 4
    comma = 3
      while comma < integer.to_s.length
        integer = integer.to_s.reverse.insert(comma,",").reverse!
        comma += 4
      end
    p integer.to_s
  end
end

# separate_comma(123)        # => "123"
# separate_comma(1234)       # => "1,234"
# separate_comma(12345)      # => "1,234,567,890"
# separate_comma(1234567890) # => "100,200,300,400,500,600,700"


# 3. Reflection
# What was your process for breaking the problem down? What different approaches did you consider?
# => I considered using the `.slice` method, but I'm less familiar with that than `.insert`

# Was your pseudocode effective in helping you build a successful initial solution?
# => It definitely made for an excellent reference once you begin laying down code.

# What new Ruby method(s) did you use when refactoring your solution? Describe your experience of using the Ruby documentation to implement it/them (any difficulties, etc.). Did it/they significantly change the way your code works? If so, how?
# => I initially refactored by looping over each comma grouping in order to cover all positive integer cases with correct output. Upon further research, I did a second refactoring using the

# How did you initially iterate through the data structure?
# => I began at the decimal point, and counting backwards from `-1` (the ones' position) I had my code insert a comma at position -4 for cases requiring only 1 comma. I did the same at position -8 for cases requiring a second comma.

# Do you feel your refactored solution is more readable than your initial solution? Why?
# => The refactored solution is not only more readable, but it also covers all integer cases but incrementing with the length of much larger integers.


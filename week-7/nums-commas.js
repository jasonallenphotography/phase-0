// Separate Numbers with Commas in JavaScript **Pairing Challenge**


// I worked on this challenge with: Emmett

// Pseudocode
// Input: Integer
// Output: String of comma separated numbers from the input Integer
// Steps: 1. Define a variable inputNum
// Step: 2. Split each indexed number into array of numbers
// Step: 3. While inputNum length is > 3
// Step: 4. Pop the last 3 and add a comma
// step: 5. Else pop the last 3 digits
// Step: 6. Output


// Initial Solution
// var inputNum = 1569743;
// var outputNum = [];
// var step = "";
// inputNum = inputNum.toString().split("");

// while (inputNum.length > 3) {
//   for (step = 0; step < 3; step++) {
//   var x = inputNum.pop();
//   outputNum.push(x);}
//   outputNum.push(",");}

// while (inputNum.length < 3 && inputNum.length > 0){
//   var x = inputNum.pop();
//   outputNum.push(x)};

//   console.log(outputNum.reverse().join(""));

// Refactored Solution

function numsCommas(inputNum){

var outputNum = [];
var x = "";
inputNum = inputNum.toString().split("");

while (inputNum.length > 3) {
  for (var step = 0; step < 3; step++) {
  x = inputNum.pop();
  outputNum.push(x);}
  outputNum.push(",");}

  outputNum.push(inputNum.join(""));

console.log(outputNum.reverse().join(""));
}

//Driver code
numsCommas(11569743) //should display `11,569,743`
numsCommas("Britain") //should give an Error

// console.log(inputNum.toLocaleString()); SUPER simple refactoring/built in method that accomplishes all of the above.

//
// Your Own Tests (OPTIONAL)
//
function assert(test, message, test_number) {
  if (!test) {
    console.log(test_number + "false");
    throw "ERROR: " + message;
  }
  console.log(test_number + "true");
  return true;


assert(
  (outputNum instanceof Array),
  "The value of inputNum has to be an integer",
  "1. "
)}


// Reflection

// What was it like to approach the problem from the perspective of JavaScript? Did you approach the problem differently?
//  For our initial solution, Emmett and I built a similar solution to our prior Ruby solutions. We certainly had to take extra steps to explicitly define variables in JS, but beyond that and some syntax differences, we came up with a similar solution.


// What did you learn about iterating over arrays in JavaScript?
// In JS there are no destructive iterating methods for arrays - one must use a new array to contain the modified results from the original.

// What was different about solving this problem in JavaScript?
//  Not much, but it was more tedious to program in JS.


// What built-in methods did you find to incorporate in your refactored solution?
//  This challenge could be solved in a single line by the built in method *.toLocaleString()). Otherwise, we refactored our solution by tidying up our code, removing extraneous methods, and structuring our program as a function with driver code calls and arguments.
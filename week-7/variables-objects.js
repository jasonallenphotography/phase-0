 // JavaScript Variables and Objects

// I paired [by myself] on this challenge.

// __________________________________________
// Write your code below.

var secretNumber = 7, password = "just open the door", allowedIn = false,members = ["John","","","Mary"];

// __________________________________________

// Test Code:  Do not alter code below this line.

function assert(test, message, test_number) {
  if (!test) {
    console.log(test_number + "false");
    throw "ERROR: " + message;
  }
  console.log(test_number + "true");
  return true;
}
//secretNumber strict equals a number
assert(
  (typeof secretNumber === 'number'),
  "The value of secretNumber should be a number.",
  "1. "
)
//secretNumber strict equals 7
assert(
  secretNumber === 7,
  "The value of secretNumber should be 7.",
  "2. "
)
//password strict equals a string
assert(
  typeof password === 'string',
  "The value of password should be a string.",
  "3. "
)
//password strict equals "just open the door"
assert(
  password === "just open the door",
  "The value of password should be 'just open the door'.",
  "4. "
)
//allowedIn strict equals a boolean
assert(
  typeof allowedIn === 'boolean',
  "The value of allowedIn should be a boolean.",
  "5. "
)
//allowedIn strict equals false
assert(
  allowedIn === false,
  "The value of allowedIn should be false.",
  "6. "
)
//members is an instance of Array
assert(
  members instanceof Array,
  "The value of members should be an array",
  "7. "
)
//the first element in members must be "John"
assert(
  members[0] === "John",
  "The first element in the value of members should be 'John'.",
  "8. "
)
//the fourth element must be "Mary"
assert(
  members[3] === "Mary",
  "The fourth element in the value of members should be 'Mary'.",
  "9. "
)
 // Design Basic Game Solo Challenge

// This is a solo challenge
// MVP reqs:
//    Create at least 2 objects with properties
//    Create at least 2 functions that interact with those objects by adding or modifying the object's properties
//    Call each function (invoke the function) Make sure the output is actually what you expect!

// Your mission description:
// Overall mission:
// Goals: Find an item, the mighty Triforce of Power, and use it to beat
//    a vicious enemy - Ganon! Get the Triforce before Ganon does,
//    or darkness will rule the land of Hyrule.
// Characters: Link, Ganon
// Objects: Triforce of Power
// Functions: Move Link, patrol Ganon, report location for both, win condition IF/ELSE statement

// Pseudocode
// Define an object `link` with the properties xLoc, yLoc, triforce.
// Define an object `ganon` with the properties xLoc, yLoc, triforce.
// Define an object `triforce` with the properties xLoc, yLoc
// Define a function `ganonPatrol` that moves `ganon` randomly to an adjacent space
// Define a function `location` that will report where `link` and `ganon` are after each move
// Define a function `move` that moves `link` up/down/left/right,
//    and tells `ganon` to `ganonPatrol`, and calls `location`
// IF link finds triforce, add 1 to link's triforce inventory,
//    ELSE IF ganon finds the triforce, add 1 to ganon's triforce inventory and it's game over
//    ELSE IF link finds ganon while link has a triforce inventory of < 1 it's game over!
//    ADDED in Refactor: ELSE IF link finds the triforce and then encounters ganon, WIN!
// Loop driver code to move `link` and test the code


// Initial Code


// CHARACTER objects
//
// var link = {
//   xLoc: 0,
//   yLoc: 0,
//   triforce: 0,
// };

// var ganon = {
//   xLoc: 10,
//   yLoc: 10,
//   triforce: 0,
// };
//
// ITEM objects
//
// var triforce = {
//   xLoc: Math.floor( (Math.random() * 10 ) +1 ),
//   yLoc: Math.floor( (Math.random() * 10 ) +1 ),
// };
//
// GAME FUNCTIONS
//
// function location() {
//   console.log("Link is at " + link.xLoc + "," + link.yLoc);
//   console.log("Ganon is at " + ganon.xLoc + "," + ganon.yLoc);
// };

// function ganonPatrol() {
//     ganon.xLoc += Math.floor( Math.random() * 2 ) - 1;
//     ganon.yLoc += Math.floor( Math.random() * 2 ) - 1;
//   };

// function move(dir) {
//   if(dir === 'up'){
//     link.yLoc += 1;
//     ganonPatrol();
//     location();
//   }
//   else if(dir === 'down'){
//     link.yLoc -= 1;
//     ganonPatrol();
//     location();
//   }
//   else if(dir === 'left'){
//     link.xLoc -= 1;
//     ganonPatrol();
//     location();
//   }
//   else if(dir === 'right'){
//     link.xLoc +=1;
//     ganonPatrol();
//     location();
//   };

// GAME WIN/LOSE CONDITIONS
// if (link.xLoc === triforce.xLoc && link.yLoc === triforce.yLoc) {
//   console.log("You have found the Triforce of Power!");
//   link.triforce += 1;
// } else if (ganon.xLoc === triforce.xLoc && ganon.yLoc === triforce.yLoc) {
//   console.log("Ganon has found the Triforce of Power - the world as we know it shall end!");
//   ganon.triforce += 1;
// } else if (link.xLoc === ganon.xLoc && link.yLoc === ganon.yLoc && link.triforce < 1 ){
//   console.log("You are too weak to defeat Ganon without the Triforce!")
//   console.log("Ganon finishes you off swiftly with a single blow. GAME OVER")
// }

// };


// Refactored Code
// Refactored the program to remove triforce property from the object ganon,
//    added an additional win condition IF triforce === 1 AND then you find ganon,
//    fixed ganon's wandering by making him 'teleport' to a coordinate X = random(1-10)
//    and coordinate y = random(1-10) as his +1/0/-1 incremental directional movement wasn't previously
//    constrained to the 10x10 board.

// CHARACTER objects

var link = {
  xLoc: 0,
  yLoc: 0,
  triforce: 0,
};

var ganon = {
  xLoc: 0,
  yLoc: 0,
};

// ITEM objects

var triforce = {
  xLoc: Math.floor( (Math.random() * 10 ) +1 ),
  yLoc: Math.floor( (Math.random() * 10 ) +1 ),
};

// GAME FUNCTIONS

function location() {
  console.log("Link is at " + link.xLoc + "," + link.yLoc);
  console.log("Ganon is at " + ganon.xLoc + "," + ganon.yLoc);
};

function ganonPatrol() {
    ganon.xLoc = Math.floor( Math.random() * 10) + 1;
    ganon.yLoc = Math.floor( Math.random() * 10 ) + 1;
  };

function move(dir) {
  if(dir === 'up'){
    link.yLoc += 1;
    ganonPatrol();
    location();
  }
  else if(dir === 'down'){
    link.yLoc -= 1;
    ganonPatrol();
    location();
  }
  else if(dir === 'left'){
    link.xLoc -= 1;
    ganonPatrol();
    location();
  }
  else if(dir === 'right'){
    link.xLoc +=1;
    ganonPatrol();
    location();
  };

// GAME WIN/LOSE CONDITIONS
  if (link.xLoc === triforce.xLoc && link.yLoc === triforce.yLoc) {
    console.log("You have found the Triforce of Power!");
    link.triforce += 1;
  } else if (ganon.xLoc === triforce.xLoc && ganon.yLoc === triforce.yLoc) {
    console.log("Ganon has found the Triforce of Power - the world as we know it shall end! GAME OVER");
  } else if (link.xLoc === ganon.xLoc && link.yLoc === ganon.yLoc && link.triforce < 1 ){
    console.log("You are too weak to defeat Ganon without the Triforce!")
    console.log("Ganon finishes you off swiftly with a single blow. GAME OVER")
  } else if (link.xLoc === ganon.xLoc && link.yLoc === ganon.yLoc && link.triforce === 1 ){
    console.log("You defeated Ganon with the powerful Triforce! Hyrule is safe! YOU WIN")
  }
};


//DRIVER CODE (Inside loop makes Link move up 10, right 1, down 10, right 1. Outside loop repeats the inside loop 5 times - enough that Link will travel a 10x10 grid and find the Triforce or find Ganon)
for(var i=0; i < 5; i++){
    for(var x=0; x < 10; x++){
    move('up');
    }
    move('right');
    for(var x=0; x < 10; x++){
    move('down');
    }
    move('right');

}
return ganon.xLoc;
//OUTPUT
// week-7 [master] :> node game.js
// Link is at 0,1
// Ganon is at 3,6
// Link is at 0,2
// Ganon is at 9,10
// ...shortened...
// You have found the Triforce of Power!
// ...shortened...
// Link is at 5,8
// Ganon is at 5,8
// You defeated Ganon with the powerful Triforce! Hyrule is safe! YOU WIN

// Reflection
// What was the most difficult part of this challenge?
//    -I am so unfamiliar with the mechanics of javascript that I could not (in the time I allotted
//      for this challenge) add nearly as much functionality as I would have liked. More guidance before
//      this challenge would have been appreciated as I felt quite lost on how to make more than a simple
//      rudimentary script with randomized enemy behavior, even with hours of research.
//      Particularly: I feel that instead of the tedious group exercise we had this week,
//      it would have been far more valuable to teach us how to set up a game board that characters
//      are constrained to, how to receive and interpret user input in real time without prompts
//      or clunky driver code, how to apply a GUI/CSS vis DOM, etc.
//      This solo 'challenge' felt like teaching a kid to ride a bike with training wheels, and then
//      taking off the training wheels and asking them to do BMX stunt jumps off of a ramp.
//      There's a lot of steps missing in between our introduction to the material, and expecting us to
//      apply more than we've learned - even with about 5-10 hours of additional research and reading
//      (Ducket's JS/JQuery book, Javascript: The Good Parts, and other offline documentation) at least within
//      the scope of resources we were allowed to use for this challenge per the Phase-0 handbook.

// What did you learn about creating objects and functions that interact with one another?
//    I learned that naming conventions are important, and reinforced what we had previously
//    learned this week regarding object creation and interactions.

// Did you learn about any new built-in methods you could use in your refactored solution? If so, what were they and how do they work?
//    -no
//
// How can you access and manipulate properties of objects?
//      Throughout my program, I used the dot property accessor - ganon.xLoc, link.triforce, etc.
//      There's also bracket notation, but I chose not to use that format as I found dot notation
//      more readable - i.e. ganon[xLoc], link[triforce]
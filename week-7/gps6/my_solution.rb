# Virus Predictor

# I worked on this challenge with: Josh Wu.
# We spent [2] hours on this challenge.

# EXPLANATION OF require_relative
# Require_relative loads in a folder from a file relative to the active ruby file. This is an external file. Require loads in a file from the ruby library

require_relative 'state_data'

class VirusPredictor

   #.new is a class method which calls the instance method initialize
  def initialize(state_of_origin, population_density, population)
    @state = state_of_origin
    @population = population
    @population_density = population_density
  end


  def virus_effects
    #Assigns data from the external Hash to a instance variables within VirusPredictor
      predicted_deaths
      speed_of_spread
  end


  private
    #Sets named methods below to private visibility

  def predicted_deaths
    # Prints predicted deaths, which are a calculated percentage of the population, based on differing population density categories, and rounded down (*.floor) to the nearest lower whole integer

    #REFACTORED IF/ELSIF/ELSE TO CASE
        # if @population_density >= 200
        #   number_of_deaths = (@population * 0.4).floor
        # elsif @population_density >= 150
        #   number_of_deaths = (@population * 0.3).floor
        # elsif @population_density >= 100
        #   number_of_deaths = (@population * 0.2).floor
        # elsif @population_density >= 50
        #   number_of_deaths = (@population * 0.1).floor
        # else
        #   number_of_deaths = (@population * 0.05).floor
        # end

    case @population_density
      when  200...99999999999999 then number_of_deaths = (@population * 0.4).floor
      when 150...200 then number_of_deaths = (@population * 0.3).floor
      when 100...150 then number_of_deaths = (@population * 0.2).floor
      when 50...100 then number_of_deaths = (@population * 0.1).floor
      when 0...50 then number_of_deaths = (@population * 0.05).floor
      else puts "error"
    end

    print "#{@state} will lose #{number_of_deaths} people in this outbreak"

  end


  def speed_of_spread #in months
    # This defines how fast the outbreak will spread. More densely states will be affected by the spread of the outbreak more quickly.
    #Prints the speed of the estimated spread.

        #REFACTORED IF/ELSIF/ELSE TO CASE
          # if @population_density >= 200
          #   speed += 0.5
          # elsif @population_density >= 150
          #   speed += 1
          # elsif @population_density >= 100
          #   speed += 1.5
          # elsif @population_density >= 50
          #   speed += 2
          # else
          #   speed += 2.5
          # end

      speed = 0.0

    case @population_density
      when 200...99999999999999 then speed += 0.5
      when 150...200 then speed += 1
      when 100...150 then speed += 1.5
      when 50...100 then speed += 2
      when 0...50 then speed += 2.5
      else puts "error"
    end


    puts " and will spread across the state in #{speed} months.\n\n"

  end

end
#     PSEUDOCODE for
      # INPUT: Target hash "STATE_DATA"
      # OUTPUT: Report of virus_effects for each state(key values in STATE_DATA)
      #  Steps: Receive hash
      #  Iterate over hash
      #  For each key create an instance of VirusPredictor
      #  and output each instance's virus_effects


  def predict_all
    STATE_DATA.each_key do |key|
      state = VirusPredictor.new(key, STATE_DATA[key][:population_density], STATE_DATA[key][:population])
      state.virus_effects
    end
  end

#=======================================================================

# DRIVER CODE
 # initialize VirusPredictor for each state

predict_all

# commented out to test `predict_all`
# alabama = VirusPredictor.new("Alabama", STATE_DATA["Alabama"][:population_density], STATE_DATA["Alabama"][:population])
# alabama.virus_effects

# jersey = VirusPredictor.new("New Jersey", STATE_DATA["New Jersey"][:population_density], STATE_DATA["New Jersey"][:population])
# jersey.virus_effects

# california = VirusPredictor.new("California", STATE_DATA["California"][:population_density], STATE_DATA["California"][:population])
# california.virus_effects

# alaska = VirusPredictor.new("Alaska", STATE_DATA["Alaska"][:population_density], STATE_DATA["Alaska"][:population])
# alaska.virus_effects


#=======================================================================
# Reflection Section
# What are the differences between the two different hash syntaxes shown in the state_data file?
# => The symbols contained in each sub-hash allow for the program to use consistent syntax throughout the calculations. The strings for the state names/keys makes printing the state names easier and cleaner for the program's output. Symbols are immutable and can't be changed once the program runs.

# What does require_relative do? How is it different from require?
# => require_relative checks for and references a file external to the program. require looks in the Ruby library i.e. `require "my_library.rb"` for ruby extensions.

# What are some ways to iterate through a hash?
# => We used `.each_key` do-loop to iterate through the Hash of hashes. There are a number of other methods available like `*.has_key?`, `*.each_value` `*.each`.

# When refactoring virus_effects, what stood out to you about the variables, if anything?
# => The arguments were all instance variables, and receiving them as arguments on the methods `predicted_deaths` and `speed_of_spread` was redundant. We removed arguments from those methods to refactor `virus_effects` as they already referenced the instance variables locally. We further refactored the methods `predicted_deaths` and `speed_of_spread` from If/Elsif/Else statements to more concise, DRY case statements.

# What concept did you most solidify in this challenge?
# => I had never worked with code that referenced an externally stored dataset before. Building the 50-state report method `predict_all` helped to solidify that workflow. Also, this was good practice iterating over hashes and refactoring.
Introduction
Did you learn anything new about JavaScript or programming in general?
  -I did not know JavaScript began as a language specific to Netscape Navigator.
Are there any concepts you feel uncomfortable with?
  -Not at this time.


Ch. 1: Values, Types, and Operators
Identify two similarities and two differences between JavaScript and Ruby syntax with regard to numbers, arithmetic, strings, booleans, and various operators.
  -All of the math operators (+,-,/,*,%,>,<) are the same between languages. Assignment operator (=), logical operators (&&, ||), and equals/strict equals (==,===) are the same as well. Null in JS is the same as Nil in Ruby with a different name. Numbers in JS are handled as floating point values by default, whereas they are handled as whole integers in Ruby.


Ch. 2: Program Structure
What is an expression?
  -An expression is a code fragment that produces a value.

What is the purpose of semicolons in JavaScript? Are they always required?
  -Semicolons in JS are not always required, but it is good practice to terminate expressions and statements with a semicolon. Semicolons end code statements.

What causes a variable to return undefined?
  -Empty variables return undefined.

What does console.log do and when would you use it? What Ruby method(s) is this similar to?
  -Log is akin to Ruby's `puts` method. It outputs an object's value to the console.

Describe while and for loops
  -A while loop loops until the while condition is no longer met. For loops allow for iterating over an array.

What other similarities or differences between Ruby and JavaScript did you notice in this section?
  -Var in JS can define multiple variables as comma separated statements on the same line

See my `eloquent.js` file for solutions to Triangle and FizzBuzz.


Ch. 3: Functions
What are the differences between local and global variables in JavaScript?
  -Local variables in JS are variables defined within a function. Any variable defined outside of a function are global, and accessible throughout the rest of your program.

When should you use functions?
  -Functions allow for definitions of specific and complex tasks that are called concisely later.

What is a function declaration?
  -A function declaration is a shorter way to define a function, its attributes, and its behavior.

See my `eloquent.js` file for the `Minimum` function.


Ch. 4: Data Structures: Objects and Arrays

What is the difference between using a dot and a bracket to look up a property? Ex. array.max vs array["max"]
  -Dot notation is one way of looking up a property in JS, but it can also be used to call named properties. array.max is calling the named function `max` on the array. array["max"] is square bracket access notation, and will print the value of the property "max".

What is a JavaScript object with a name and value property similar to in Ruby?
  -Hashes!


See my `eloquent.js` file for my `me` object!
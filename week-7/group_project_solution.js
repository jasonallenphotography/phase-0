
//User Story by kevin

// As a user, I want to be able to input any list of numbers and find the following values for the items in the list: the SUM, the MEAN and the MEDIAN.  The input can be a list of numbers with an odd or even length, but if the length of the list is even, the median needs to return a float value of the middle of the list (for example, a list containting 1, 2, 3, and 4 would return 2.5 as a median.)

 // User Stories to Pseudocode by Li-Lian

 // Create object that takes an input of array.

 // Create function for SUM that takes the array object as an argument. Use for loop to iterate through each array element:
 // For var num equal zero, stop when less than array length and then increments by 1. Add numbers up to get SUM.

 // For MEAN function, divide total of SUM by length of input array object to calculate MEAN.

 // For MEDIAN function,
  // If the length of the array object is even, find the two middle elements, sum them up and and divide it by two.
 // else
  // Find middle element of the array.


// Initial Solution-- by Megan and Joseph


// var listOfNums = [1,2,3,55,7,9,3,6];

// var sumAll = function(array) {
//   var total = 0;
//   for(var i in array) {
//     total += array[i];
//   }
//   console.log(total);
// }


// function mean(array) {
//   var total = 0;
//   var i;
//     for (i = 0; i < array.length; i += 1) {
//         total += array[i];
//     }
//     console.log(total / array.length);
// }

// function median(array) {
//   array.sort(function(a,b){return a - b});
//   if(array.length % 2 === 0) {
//     return ( array[(array.length/2)-1] + array[(array.length/2)] ) / 2
//   } else {
//     return array[(array.length-1)/2]
//   }
// }

// Driver code
// sumAll(listOfNums);
// mean(listOfNums);
// console.log(median(listOfNums));



// RELEASE 4: REFACTOR and RELEASE 5 both by Jason Allen

// Refactored variable names for logic transparency and readability.
// Refactored function definitions to be consistent.
// Refactored instances of `console.log` to `return` for consistency and DRYer driver code.
// Refactored the function `mean` to tremendously improve DRYness.

// My interpretation of the user story based on the code? As a user, I'd like to take a list of
// numbers and do the following: Calculate a sum of the data, calculate the mean value of the
// data, and locate the median - even if there is an even number of entries in the data set
// (which would require calculating the average of the two middle values).

// Which tests pass? Which tests fail? Write a summary of what passed and what failed as a comment:
// The function `sumAll` was incorrectly named, and failed test 1, but that was easily corrected by
// changing its name to `sum`. Test 5 failed, as its output was being printed with `console.log`
// instead of `return`. This was corrected in refactoring.


var listOfNums = [1,2,3,55,7,9,3,6];

function sum(array) {
  var total = 0;
  for(var number in array) {
    total += array[number];
  }
  return total;
}

function mean(array) {
    return (sum(array) / array.length);
}

function median(array) {
  array.sort(function(a,b){return a - b});
  var length = array.length; // GREATLY improves readability for all of the frequent future calls of `array.length`
  if(length % 2 === 0) {
    return ( array[(length/2)-1] + array[(length/2)] ) / 2
  } else {
    return array[(length-1)/2]
  }

}

// Driver code
sum(listOfNums);
mean(listOfNums);
median(listOfNums);



// Add the finished solution here when you receive it.
// __________________________________________
// Tests:  Do not alter code below this line.


oddLengthArray  = [1, 2, 3, 4, 5, 5, 7]
evenLengthArray = [4, 4, 5, 5, 6, 6, 6, 7]


function assert(test, message, test_number) {
  if (!test) {
    console.log(test_number + "false");
    throw "ERROR: " + message;
  }
  console.log(test_number + "true");
  return true;
}

// tests for sum
assert(
  (sum instanceof Function),
  "sum should be a Function.",
  "1. "
)

assert(
  sum(oddLengthArray) === 27,
  "sum should return the sum of all elements in an array with an odd length.",
  "2. "
)

assert(
  sum(evenLengthArray) === 43,
  "sum should return the sum of all elements in an array with an even length.",
  "3. "
)

// tests for mean
assert(
  (mean instanceof Function),
  "mean should be a Function.",
  "4. "
)

assert(
  mean(oddLengthArray) === 3.857142857142857,
  "mean should return the average of all elements in an array with an odd length.",
  "5. "
)

assert(
  mean(evenLengthArray) === 5.375,
  "mean should return the average of all elements in an array with an even length.",
  "6. "
)

// tests for median
assert(
  (median instanceof Function),
  "median should be a Function.",
  "7. "
)

assert(
  median(oddLengthArray) === 4,
  "median should return the median value of all elements in an array with an odd length.",
  "8. "
)

assert(
  median(evenLengthArray) === 5.5,
  "median should return the median value of all elements in an array with an even length.",
  "9. "
)
// Eloquent JavaScript

// Run this file in your terminal using `node my_solution.js`. Make sure it works before moving on!

// Program Structure
// Write your own variable and do something to it.
var x = 20;
x += 1;

// Write a short program that asks for a user to input their favorite food.
function favoriteFood() {
var food = prompt("What's your favorite food?");
console.log("Hey! That's my favorite too!");
}
favoriteFood();

// Complete one of the exercises: Looping a Triangle, FizzBuzz, or Chess Board
// Triangle loop
function triangle(size) {
var fill = "#";
var fillNext = [];
    for (var i = 0; i < size + 1; i++) {
    console.log(fillNext.join());
    fillNext.push(fill);
    };
}
triangle(3);

//FizzBuzz
function fizzBuzz(maxNumber) {
var number = 0;
while (number <= maxNumber) {
    if (number % 3 == 0) {
      console.log("Fizz");
      number += 1;
    } else if (number % 5 == 0) {
      console.log("Buzz");
      number += 1;
    } else {
      console.log(number);
      number +=1;
    }
  }
}
fizzBuzz(100);


// Functions
// Complete the `minimum` exercise.
function minimum(a,b){
var output = (a < b) ? a : b;
console.log(output);
}
minimum(10,20);

// Data Structures: Objects and Arrays
// Create an object called "me" that stores your name, age, 3 favorite foods, and a quirk below.

var me = {
  name: "Jason"
  age: 30
  favoriteFoods: ["Pizza", "Pasta", "Steak"]
  quirk: "I talked my way onto my college's pep band as an electric guitar player, and was the only electric guitar player for a Division 1 NCAA men's basketball team."
};
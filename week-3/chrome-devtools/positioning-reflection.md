#DBC Exercise 3.4 Reflection


##How can you use Chrome's DevTools inspector to help you format or position elements?
Chrome's DevTools inspector lets you quickly view what CSS element is affecting a corresponding HTML div. Having them side by side in a GUI allows the developer to try format or position changes quickly and see immediate feedback.

##How can you resize elements on the DOM using CSS?
There are a number of properties allowing a developer to change the size of DOM elements. Using CSS, one can change the width, height, top, bottom, left, and right position, in addition to globally scaling elements. These properties can be in pixels (px), percent (relative to the viewport's dimensions), em (relative to the font size in use in the affected element, i.e. 3.5em = 3.5 times the height of the current font), and other methods relative to the viewport size.

##What are the differences between absolute, fixed, static, and relative positioning? Which did you find easiest to use? Which was most difficult?
Static positioning is where an object would be placed, as defined by the HTML.

Relative positioning adjusts and object's placement *relative* to its static position.

Absolute position is completely defined by the CSS, which is useful for placing objects flush with the side, top, or bottom of a document. An Absolute positioned element with no ancestor will be positioned relative to 0,0 on the viewport, or relative to it's nearest ancestor that it inherits from. This is particularly useful for headers, sidebars, and footer elements, or pop-over advertising, but I'd imagine also useful for body elements in a liquid or fixed-grid design.

Fixed position is also completely defined by the CSS using the top, bottom, left, and right properties. Fixed elements stay locked to the viewport, even as the page is scrolled. This I'd imagine would be useful for a nav bar or ever-present side bar element.

For this exercise, static positioning was problematic (and relative by proxy), as it left padding and margin spacing between elements and the side of the viewport. Using Absolute positioning allowed for placing elements completely flush with an edge of the viewport. With their correct usage in mind, I did not find any of the positioning properties difficult, just more or less appropriate for each exercise.

##What are the differences between margin, border, and padding?
Margins are the space AROUND an object outside of its border. Padding is the space WITHIN an object, not including the dimensions of its border, between the outside edge and the content within. A border is a sometimes-stylized box around the outside of an object, and within its margin.

##What was your impression of this challenge overall? (love, hate, and why?)
Overall this challenge was useful for learning correct usage of CSS positioning, but my pair and I found the Chrome Devtools interface frustrating and tedious to use as compared to editing the CSS directly in our text editor locally and refreshing the page, which we did for our "Get Creative" positioning practice. Otherwise, CSS is starting to make sense, and we enjoyed working together!

___

![01](imgs/1_changecolors.png)
![02](imgs/2_column.png)
![03](imgs/3_row.png)
![04](imgs/4_equidistant.png)
![05](imgs/5_squares.png)
![06](imgs/6_footer.png)
![07](imgs/7_header.png)
![08](imgs/8_sidebar.png)
![09](imgs/9_ALTERNATE.png)
#Design Reflection - DBC Module 2.3
##What are the 6 Phases of Web Design?
The six phases of web design are Information Gathering, Planning, Design, Development, Testing and Delivery, Maintenance.

##What is your site's primary goal or purpose? What kind of content will your site feature?
My site's primary goal is to highlight my capabilities to prospective employers, showcase my best work, highlight my past performance and awards, display my resumé, and provide site visitors with a means to contact me directly. My site will feature case studies and samples of my past work, a PDF of my resumé, and text descriptions of my capabilities as a junior developer.

##What is your target audience's interests and how do you see your site addressing them?
I'm targeting prospective employers, and will cater the layout directly for easy access to information a hiring manager would want to know - programming languages that I'm familiar with, examples of real world application, and samples of past performance.

##What is the primary "action" the user should take when coming to your site? Do you want them to search for information, contact you, or see your portfolio? It's ok to have several actions at once, or different actions for different kinds of visitors.
The primary action I want users to take when visiting my site would be to learn more about me as a person, and quickly learn what I am capable of professionally. Contacting me with job inquiries would be the end goal.

##What are the main things someone should know about design and user experience?
A site should load quickly and display the information parsimoniously and beautifully. In my opinion, nothing important should be out of sight when the page loads - Assume that all users are lazy and won't look far for any feature that isn't obvious. Anything important should be "front page, above the cut" in newspaper terms; plainly visible and eyecatching when the page loads.

##What is user experience design and why is it valuable?
User experience design is design based upon how a user feels when they interact with your site. Accessibility, and utility of your site drives how easily someone can use your design. Poor UX experience will drive away users you hope to engage with!

##Which parts of the challenge did you find tedious?
Some of the tools I utilized for site mapping were tedious. I ended up using Powerpoint to create my sitemap.

My site map:
![JCA Site Map](imgs/site-map.png)

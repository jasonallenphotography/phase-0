#My index.html wireframe:
![JCA index.html wireframe](imgs/wireframe-index.png)

#My blog/index.html wireframe:
![JCA blog/index.html wireframe](imgs/wireframe-blog-index.png)


##What is a wireframe?
A wireframe is a visual representation of how you imagine a UX design will ultimately look once coded.

##What are the benefits of wireframing?
Wireframing allows you to move elements around and figure out design choices quickly without having to code the changes.

##Did you enjoy wireframing your site?
Yes, I enjoyed wireframing my site - I have a background in design, and have done print and motion graphics design professionally for the past ten years.

##Did you revise your wireframe or stick with your first idea?
I went back to the drawing board a few times! Typically I design something, take a break, and revisit it after a meal or doing another task, and then redesign it.

##What questions did you ask during this challenge? What resources did you find to help you answer them?
I did not have questions on this challenge.

##Which parts of the challenge did you enjoy and which parts did you find tedious?
I forgot to incorporate my blog into the site map from exercise 2.3, and I had to re-design my sitemap and wireframes accordingly... so that extra step that I made for myself was tedious! UX design is one of my favorite parts of this field so far - I enjoyed most of this challenge as a result.
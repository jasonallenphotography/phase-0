# Build a simple guessing game


# I worked on this challenge by myself.
# I spent [2] hours on this challenge.

# Pseudocode

# Input: Integer argument `answer`
# Output: High, Low, or Correct
# Steps:
# => Receive the integer argument `answer` and initialize a new instance of the GuessingGame.
# => IF user input for the method `guess` is equal to `answer`, RETURN :correct
# => ELSIF user input for the method `guess` is above `answer`, RETURN :high
# => ELSIF user input for the method `guess` is below `answer`, return :low
# => ELSE RETURN
# => Method `solved?` returns true if most recent guess is true, false if the most recent guess is false.

# Initial Solution
=begin
class GuessingGame

  def initialize(answer)
    @answer = answer.to_i
  end

  def guess(attempt)
    @last_guess = attempt

    if attempt == @answer
      return :correct
    elsif attempt > @answer
      return :high
    else
      return :low
    end
  end

  def solved?
    if @last_guess == @answer
      return true
    elsif @last_guess != @answer
      return false
    end
  end
end

=end


# Refactored Solution
# IMPLICIT returns throughout, simplified if
class GuessingGame

  def initialize(answer)
    @answer = answer.to_i
  end

  def guess(attempt)
    @last_guess = attempt

    if attempt == @answer
      :correct
    elsif attempt > @answer
      :high
    else
      :low
    end
  end

  def solved?
    @last_guess == @answer ? true : false
  end
end

game = GuessingGame.new(10)
game.solved?   # => false

game.guess(5)  # => :low
game.guess(20) # => :high
game.solved?   # => false

game.guess(10) # => :correct
game.solved?   # => true

# Reflection
=begin
# How do instance variables and methods represent the characteristics and behaviors (actions) of a real-world object?
Instance variables and methods use the method to describe the properties of an object, but each instance, while the same type of object, is its own object, and its own variable.


# When should you use instance variables? What do they do for you?
If `Human` were a programmed class, jason = Human.new() is a different instance from susy = Human.new(). We could have each Human instance performing different functions without affecting the other instance, i.e. jason.sleep(8), susy.eat("bagel").


# Explain how to use flow control. Did you have any trouble using it in this challenge? If so, what did you struggle with?
I look at flow control as gates - if a condition is met, a gate opens allowing the process to the next gate. If a condition is failed, the process ends, or it redirects in another direction. I did not have issues with flow control here.


# Why do you think this code requires you to return symbols? What are the benefits of using symbols?
Symbols are unique and can't be changed once the program runs - which is useful for the :high, :correct, and :low symbols. They are also quick to recall, which enhances performance.


=end
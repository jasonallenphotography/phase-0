#Attr Methods

# I worked on this challenge by myself

# I spent [2] hours on this challenge.

# Pseudocode

# Input: Name as an argument
# Output: String "Hello #{name}! How wonderful to see you today."
# Steps:
# => Create a class NameData with an accessible variable `name`
# => Create a class Greetings with method `hello` that accepts an argument and outputs a string.
# => Use driver code to call a new instance of NameData and give it a `:name`
# => Use driver code to call a new instance of Greetings
# => Use driver code to call the Greetings method `hello` and give it the instance of NameData as an argument

class NameData
attr_accessor :name
  def initialize(name)
    @name = name
  end
end

class Greetings
  def hello(name)
    puts "Hello #{name}! How wonderful to see you today."
  end
end

p1 = NameData.new("John")
greet = Greetings.new
greet.hello(p1.name)

# Reflection
=begin

Release 1
What are these methods doing?
# => `initialize` creates a new instance of `Profile`.
# => `print_info` outputs the instance variables @age, @name, @occupation
# => `what_is_age` implicitly returns @age
# => `change_my_age=(new_age)` resets @age to argument new_age
# => `what_is_name` implicitly returns @name
# => `change_my_name=(new_name)` resets @name to argument new_name
# => `what_is_occupation` implicitly returns @occupation
# => `change_my_occupation=(new_occupation)` resets @occupation to argument new_occupation

How are they modifying or returning the value of instance variables?
# => Modifications are made through variable assignment, and returns are for the most part implicit except where `puts` is used.



Release 2
What changed between the last release and this release?
# => An attr_reader has been added to allow the instance variable @age to be read externally in the runtime.

What was replaced?
# => The method `what_is_age` can now be replaced by calling `p instance_name.age`

Is this code simpler than the last?
# => It is somewhat simpler, yes. 3 lines of a method calling @age has been reduced to 1 line.



Release 3
What changed between the last release and this release?
# => There is now an `attr_writer` call on line 6, which obviates the need for the method `change_my_age`

What was replaced?
# => Instead of calling the method `change_my_age`, the attr_writer allows the developer to change the value of @age by `instance_of_profile.age = 28`, which is more concise driver code.

Is this code simpler than the last?
# => Yes, it is. It's again shorter by reducing unnecessary methods.



Release 6
What is a reader method?
# => A reader method allows an instance variable to be read from outside of the method or class it is contained in.

What is a writer method?
# => A writer method allows an instance variable to be written to, but not read from outside of the method or class it is contained in.

What do the attr methods do for you?
# => attr methods allow you to tremendously shorten the amount of repetitive code and instance variables you need to write. It makes for much DRYer code.

Should you always use an accessor to cover your bases? Why or why not?
# => attr_accessor allows both read and write privileges, which may pose a security risk. Sometimes write-only, and read-only cases are appropriate to compartmentalize data.

What is confusing to you about these methods?
# => Initially, I had forgotten to create an instance of the first class to call variables from in the second class & method called.


=end
# RELEASE 2: NESTED STRUCTURE GOLF
# Hole 1
# Target element: "FORE"

array = [[1,2], ["inner", ["eagle", "par", ["FORE", "hook"]]]]

# attempts:
# ============================================================
array[1][1][2][0]
# Attempted refactoring... decided our original way worked best!
# array.value_at()

# ============================================================

# Hole 2
# Target element: "congrats!"

hash = {outer: {inner: {"almost" => {3 => "congrats!"}}}}

# attempts:
# ============================================================
p hash[:outer][:inner]["almost"][3]
# ============================================================

# Hole 3
# Target element: "finished"

nested_data = {array: ["array", {hash: "finished"}]}

# attempts:
# ============================================================
p nested_data[:array][1][:hash]
# ============================================================

# RELEASE 3: ITERATE OVER NESTED STRUCTURES

number_array = [5, [10, 15], [20,25,30], 35]
number_array.each do |number|
  if number.is_a?(Array)
      number.each do |value|
        p value
      end
  else
        p number
  end
  puts
end
# REFACTOR:
number_array.flatten!.each { |number| p number }

# Bonus:

startup_names = ["bit", ["find", "fast", ["optimize", "scope"]]]
coverted = startup_names.flatten
p coverted.map! { |word| word + "ly"}

# What are some general rules you can apply to nested arrays?
# => Nested arrays are tedious and should be avoided.

# What are some ways you can iterate over nested arrays?
# => You can iterate over an array, declare an if statement to check if the value is a nested array, do a task for all nested arrays, else, do the same task for all non-array values. Or you can refactor and flatten the nested structure in cases where appropriate!

# Did you find any good new methods to implement or did you re-use one you were already familiar with? What was it and why did you decide that was a good option?
# => Using `flatten` made it easier to modify all of the contained array elements by removing the multidimensional structure.
=begin
# Your Names
# 1) Erica Lloyd
# 2) Jason Allen

# We spent [1.5] hours on this challenge.

# Bakery Serving Size portion calculator.

      # Defines a method that receives two arguments, item_to_make and num_of_ingredients
def serving_size_calc(item_to_make, num_of_ingredients)
      # Establishes a Hash for the items to make, and the corresponding ingredient use
  library = {"cookie" => 1, "cake" =>  5, "pie" => 7}
      # Establishes an decremental error counter
  error_counter = 3
      # Iterate through the library to see if any key matches item_to_make
  library.each do |food|
    if key/value pair is not equal to item_to_make
    then decrement error_counter
    p library[food]
    if library[food] != library[item_to_make]
      error_counter += -1
    end
  end

  if library.has_key?(item_to_make) == false
    raise ArgumentError.new("#{item_to_make} is not a valid input")
  end

      #if error_counter has not reached 0 then
      # raise error that item_to_make is not in
      #the library


      #Assign num_of_ingredients to serving_size
      #Assign remainder to remaining_ingredients
  serving_size = library.values_at(item_to_make)[0]
  remaining_ingredients = num_of_ingredients % serving_size

  when remaining_ingredients = 0
  #  returns a calculation of num_of_ingredients divided by serving size out of total item_to_make else returns a calculation of num_of_ingredients divided by serving size out of total item_to_make and provide total of remaining_ingredients
  case remaining_ingredients
  when 0
    return "Calculations complete: Make #{num_of_ingredients / serving_size} of #{item_to_make}"
    else
      return "Calculations complete: Make #{num_of_ingredients / serving_size} of #{item_to_make}, you have #{remaining_ingredients} leftover ingredients. Suggested baking items: TODO: MAKE THIS FEATURE"
  end
end


  output =  "Calculations complete: Make #{num_of_ingredients / serving_size} of #{item_to_make}"
  remaining_output = ", you have #{remaining_ingredients} leftover ingredients. Suggested baking items: TODO: MAKE THIS FEATURE"

  if remaining_ingredients > 0
    output += remaining_output
  end
  return output
end

=end

#REFACTOR

def recipe_batch_calc(food, num_of_ingredients)
  recipe = {"cookie" => 1, "cake" =>  5, "pie" => 7}

  if recipe.has_key?(food) == false
    raise ArgumentError.new("#{recipe} is not a valid input")
  end

  num_batches = recipe[food]
  remaining_ingredients = num_of_ingredients % num_batches

  output =  "Calculations complete: Make #{num_of_ingredients /   num_batches} of #{food}"
  remaining_output = ", you have #{remaining_ingredients} leftover ingredients."

  if  num_of_ingredients < recipe[food]
      puts "Go to the grocery store!"
        if remaining_ingredients > 0
        output += remaining_output
        end
  end
  return output
end

# Driver test code
p recipe_batch_calc("pie", 3)
p recipe_batch_calc("pie", 7)
p recipe_batch_calc("pie", 8)
p recipe_batch_calc("cake", 5)
p recipe_batch_calc("cake", 7)
p recipe_batch_calc("cookie", 1)
p recipe_batch_calc("cookie", 10)
# p serving_size_calc("THIS IS AN ERROR", 5)

# #  Reflection
# What did you learn about making code readable by working on this challenge?
# => Transparent naming is critical, parsimonious code is important, and repetition is a waste of space!

# Did you learn any new methods? What did you learn about them?
# => I learned that .has_key?() is a synonym of .includes?() but a more descriptive one!

# What did you learn about accessing data in hashes?
# => Just reinforced what I knew.

# What concepts were solidified when working through this challenge?
# => Refactoring large sections into a single, simple line of code, reading for control flow, and pseudocode were all reinforced. As Erica and I refactored this file, we used pseudocode a lot to discuss how we wanted our methods to flow logically before implementing them. We refactoring in about 75 minutes total, including adding a new feature - an if statement that tells the coding baker to go to the grocery store if they have insufficient ingredients!
# Die Class 2: Arbitrary Symbols


# I worked on this challenge by myself.
# I spent [1.5] hours on this challenge.

# Pseudocode

# Input: An array of strings
# Output: Random string from the input array
# Steps:
# Define a method that initializes an array of strings. Define a method that determines the number of strings in the array and returns/prints the result. Define a method that randomly selects one of the strings from the input array and returns/prints the result.


# Initial Solution

# class Die
#   def initialize(labels)
#     @sidename = Array.new(labels)
#     if @sidename.length < 1
#       raise ArgumentError.new("The die must have at least 1 side!")
#     else
#       return
#     end
#   end

#   def sides
#     p @sidename.size
#   end

#   def roll
#     result = @sidename.sample(random:1)
#       puts "You rolled #{result.to_s}."
#     p result
#   end
# end

# die = Die.new(["A","B","C","D","E","F","G"])
# die.sides
# die.roll

# Refactored Solution

class Die
  def initialize(labels)
    @sidename = Array.new(labels)
    if @sidename.length < 1
      raise ArgumentError.new("The die must have at least 1 side!")
    else
      return
    end
  end

  def sides
    p @sidename.size
  end

  def roll
    p @sidename.sample(random:1)
  end
end

die = Die.new(["A","B","C","D","E","F","G"])
die.sides
10.times do die.roll end

# Reflection
# What were the main differences between this die class and the last one you created in terms of implementation? Did you need to change much logic to get this to work?
# => This die class required a different method of random selection. In last week's exercise, I used 'result = rand(1..@sides)', as the variable '@sides' defined the total number of sides. This die class received an array as input, allowing for the '.sample(random:1)' to iterate over the whole array beginning to end, and randomly select one value of the array.

# What does this exercise teach you about making code that is easily changeable or modifiable?
# => Sometimes it's necessary to change the mechanics of how a method works - having transparent, easily readable code allows a developer to quickly redefine the workings of a method.

# What new methods did you learn when working on this challenge, if any?
# =>This is the first time I've used the .sample() method, and also my first time using the random operator.

# What concepts about classes were you able to solidify in this challenge?
# => Remembering to use a class variable was a valuable concept to revisit.
# Class Warfare, Validate a Credit Card Number


# I worked on this challenge with: Brian Mosley.
# I spent [2] hours on this challenge.

# Pseudocode

# Input: Single string argument
# Output: Boolean True or False if the string is or is not a valid credit card number
# Steps:
#    Map the string input to an array
#    Iterate over the array and double every other value beginning at position (-2 back to the first digit)
# Split every doubled number so that we can work with them individually
#    Sum of all values in the array
#    Check the array against % 10, which should equal 0 if true


# Initial Solution

# Don't forget to check on initialization for a card length
# of exactly 16 digits


=begin

class CreditCard

    def initialize(test_number)
        if test_number.to_s.length != 16
            raise ArgumentError.new("That's not a valid number!")
                # rejects integer arguments that aren't 16 characters long
        else
            @test_number = test_number
                # creates a class variable receiving the passing arguments
        end
    end

    def check_card
        cardcheck = []
            # creates a new array named cardcheck
        cardcheck = @test_number.to_s.split("")
            # receives @test_number, converts to string, splits at every character
        cardcheck.map! {|x| x.to_i }
            # maps all values (destructively) back to integers

        p cardcheck

        counter = -2
            # creates a counter that will be used for position assignment
        until counter < -16
            cardcheck[counter.to_i] *= 2
            counter -= 2
        end
            # Until counter exceeds the maximum length of a valid credit card, it will work backwards from the second to last position [-2, or counter's initial value], doubling each value of cardcheck[] in place, and decrementing counter to the next position from the rear -2, -4, etc until counter < -16.

        p cardcheck
          cardcheck.map! {|x| x.to_s.split("") }.flatten!
            # Takes cardcheck, converts it to a string, splits all two or larger digit values of cardcheck, and maps them to an array of individual digits, i.e. ["12"] becomes ["1","2"], and then flatten! removes the nested array structure so that [12, 8, 2] becomes ["12", "8", "2"], becomes [["1","2"], "8", "2"], becomes ["1","2","8","2"]
          cardcheck.map! {|x| x.to_i}
            #Maps the values of cardcheck back to integers
        p cardcheck

        if cardcheck.inject(:+) % 10 == 0
            p true
        else
            p false
        end
            # if statement to check if the sum of cardcheck's values is divisible by 10 evenly.
    end
end
card = CreditCard.new(4563960122001999)
card.check_card
    # Tests the program's performance on a known `true` card


=end

# Refactored Solution

class CreditCard

    def initialize(test_number)
        if test_number.to_s.length != 16
            raise ArgumentError.new("That's not a valid number!")
        else
            @test_number = test_number
        end
    end

    def check_card
        cardcheck = []
        cardcheck = @test_number.to_s.split("")
        cardcheck.map! {|x| x.to_i }

        counter = -2
        until counter < -16
            cardcheck[counter] *= 2
            counter -= 2
        end

        cardcheck.map! {|x| x.to_s.split("") }.flatten!
        cardcheck.map! {|x| x.to_i}
        cardcheck.inject(:+) % 10 == 0 ? true : false
            ### Simplified if statement to check if the sum of cardcheck's values is divisible by 10 evenly.
    end
end

card = CreditCard.new(4563960122001999)
card.check_card

# Reflection
# What was the most difficult part of this challenge for you and your pair?
# => The most difficult part was figuring out how to iterate across the array `cardcheck` so that every other value would be doubled. Implementing an 'until' loop with a decremental counter accomplished that task!

# What new methods did you find to help you when you refactored?
# => *.map! and *.flatten! helped tremendously for changing values in place within the data structure in the initial solution. Using a conditional if statement to evaluate whether or not the sum of cardcheck is evenly divisible by 10 made the final part of the refactored solution much more concise and simpler to read.

# What concepts or learnings were you able to solidify in this challenge?
# => Applying Pseudocode to complex operations was solidified in this exercise for me.
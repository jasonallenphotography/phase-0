# Add it up!

# Complete each step below according to the challenge directions and
# include it in this file. Also make sure everything that isn't code
# is commented in the file.

# I worked on this challenge with: Joshua Wu.

# 0. total Pseudocode
# make sure all pseudocode is commented out!
# For each value contained in an array, add the value to a variable named "sum".


# Input:
# Output:
# Steps to solve the problem.


# 1. total initial solution
def total(array)
  sum = 0
  array.each do |number|
    sum = number + sum
  end
  p sum
end

# 3. total refactored solution
def total(array)
  sum = 0
  array.flat_map { |number| sum = number + sum }
  p sum
end

# 4. sentence_maker pseudocode
# make sure all pseudocode is commented out!

# Input:
# Output:
# Steps to solve the problem.
# For each string contained in an array, concatenate for each string named word.

# 5. sentence_maker initial solution
def sentence_maker(array)
  sentence = ""
  sentence = array.first + sentence
  array.shift
  array.each {|word| sentence = sentence + " " + word.to_s }

  sentence = sentence.capitalize + "."
  p sentence
end


# 6. sentence_maker refactored solution

def sentence_maker(array)
  sentence = ""
  sentence = array.first + sentence
  array.shift
  array.collect {|word| sentence = sentence + " " + word.to_s }
  sentence = sentence.capitalize + "."
  p sentence
end
sentence_maker(["my","name","is","Bob"])
sentence_maker(["This","is","a","test"])

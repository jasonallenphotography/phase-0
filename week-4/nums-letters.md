##What does puts do?
`puts` represents `put string` and outputs only the console output - it does not return the code evaluated by Ruby as `p` does.

##What is an integer? What is a float?
An `integer` is a whole number without a decimal point, i.e. -9999, 0, 12
A `float` is a number with a decimal point, i.e. -12.2, 0.1, 5555.23

##What is the difference between float and integer division? How would you explain the difference to someone who doesn't know anything about programming?
Floating division numbers will give you the result you'd expect from arithmetic, i.e. Nine divided by two is four and a half (4.5). Integer division will deliver a somewhat unexpected result for the uninitiated. In integer division within Ruby, nine divided by two is four. Ruby rounds down to drop the decimal portion of the returned integer result!

#Links to exercises:
[4.2.1](defining-variables.rb)
[4.2.2](simple-string.rb)
[4.2.3](basic-math.rb)

##How does Ruby handle addition, subtraction, multiplication, and division of numbers?
Ruby (and most programming languages) handle arithmetic with integers by default. The language uses `+`, `-`, `*`, and `/` operators to add, subtract, multiply, and divide integer numbers, floating point numbers, and variables representing ints and floats.

##What is the difference between integers and floats?
`See above answer - assignment is redundant`

##What is the difference between integer and float division?
`See above answer - assignment is redundant`

##What are strings? Why and when would you use them?
Strings are objects that are ASCII characters. They're used for printing readable prompts and sentences, and can be modified with text operators like `.upcase` `.reverse` `.capitalize` etc.

##What are local variables? Why and when would you use them?
Local variables are used to contain a variable within an argument or method.

##How was this challenge? Did you get a good review of some of the basics?
I found this challenge very straightforward. I did not encounter any errors in `rspec` except in the basic-math.rb file (4.2.3), as I didn't note that the modulus and quotient processes were expected to be `num1.to_f % num 2.to_f` and `num1.to_f / num2.to_f`. I had reversed their order. Otherwise I still recalled correct syntax and didn't have any hiccups quickly coding the requested solutions!
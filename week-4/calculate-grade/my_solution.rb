# Calculate a Grade

# I worked on this challenge with: Julia.


# Your Solution Below
def get_grade(grade)
  if grade.to_i >= 90
    p "A"
  elsif grade.to_i >= 80 && grade.to_i <= 89
    p "B"
  elsif grade.to_i >= 70 && grade.to_i <=79
    p "C"
  elsif grade.to_i >= 60 && grade.to_i <= 69
    p "D"
  else grade.to_i < 60
    p "F"
  end
end

  get_grade(99) #Should Return "A"
  get_grade(84) #Should Return "B"
  get_grade(78) #Should Return "C"
  get_grade(69) #Should Return "D"
  get_grade(59) #Should Return "F"
  get_grade(9) #Should Return "F"

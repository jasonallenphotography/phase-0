# Analyze the Errors

# I worked on this challenge [by myself, with: ].
# I spent [#] hours on this challenge.

# --- error -------------------------------------------------------

# "Screw you guys " + "I'm going home." = cartmans_phrase

# This error was analyzed in the README file.
# --- error -------------------------------------------------------

# def cartman_hates(thing)
#   while true
#     puts "What's there to hate about #{thing}?"
# end

# This is a tricky error. The line number may throw you off.

# 1. What is the name of the file with the error?
# => errors.rb
# 2. What is the line number where the error occurs?
# => Line 170
# 3. What is the type of error message?
# =>  syntax error
# 4. What additional information does the interpreter provide about this type of error?
# => unexpected end-of-input
# 5. Where is the error in the code?
# => It is after the period:
# Write your reflection below as a comment.
#                                          ^
# 6. Why did the interpreter give you this error?
# => The `while` statement on line 14 ends on line 16, but there should be an additional `end` on line 17 to close the method definition that begins on line 13. The interpreter is searching for it all the way to the end of the document.

# --- error -------------------------------------------------------

# south_park

# 1. What is the line number where the error occurs?
# => Line 37
# 2. What is the type of error message?
# => undefined local variable or method 'south_park'
# 3. What additional information does the interpreter provide about this type of error?
# => NameError
# 4. Where is the error in the code?
# => The whole line is an error, as the variable isn't defined.
# 5. Why did the interpreter give you this error?
# => The variable named south_park has no value. This can be remedied with "south_park = value"

# --- error -------------------------------------------------------

# cartman()

# 1. What is the line number where the error occurs?
# => Line 52
# 2. What is the type of error message?
# => Undefined method `cartman'
# 3. What additional information does the interpreter provide about this type of error?
# =>  NoMethodError
# 4. Where is the error in the code?
# => Line 52 is calling an undefined method.
# 5. Why did the interpreter give you this error?
# => The method requires a definition before it is called.

# --- error -------------------------------------------------------

# def cartmans_phrase
#   puts "I'm not fat; I'm big-boned!"
# end

# cartmans_phrase('I hate Kyle')

# 1. What is the line number where the error occurs?
# => 71
# 2. What is the type of error message?
# => wrong number of arguments (1 for 0)
# 3. What additional information does the interpreter provide about this type of error?
# => ArgumentError
# 4. Where is the error in the code?
# => The method cartmans_phrase does not receive arguments, it is merely called.
# 5. Why did the interpreter give you this error?
# => When the method was called, an argument was added where none was expected.

# --- error -------------------------------------------------------

# def cartman_says(offensive_message)
#   puts offensive_message
# end

# cartman_says

# 1. What is the line number where the error occurs?
# => 86
# 2. What is the type of error message?
# =>  wrong number of arguments (0 for 1)
# 3. What additional information does the interpreter provide about this type of error?
# =>  ArgumentError
# 4. Where is the error in the code?
# => When the method is called, it requires an argument `offensive_message`
# 5. Why did the interpreter give you this error?
# => When the method was called on line 90, no `offensive_message` was given.



# --- error -------------------------------------------------------

# def cartmans_lie(lie, name)
#   puts "#{lie}, #{name}!"
# end

# cartmans_lie('A meteor the size of the earth is about to hit Wyoming!')

# 1. What is the line number where the error occurs?
# => 107
# 2. What is the type of error message?
# => wrong number of arguments (1 for 2)
# 3. What additional information does the interpreter provide about this type of error?
# => ArgumentError
# 4. Where is the error in the code?
# => On line 111, when the method is called, no argument is given for name.
# 5. Why did the interpreter give you this error?
# => The method requires two arguments be given. Only one was.

# --- error -------------------------------------------------------

# 5 * "Respect my authoritay!"

# 1. What is the line number where the error occurs?
# => 126
# 2. What is the type of error message?
# => String can't be coerced into Fixnum
# 3. What additional information does the interpreter provide about this type of error?
# =>  TypeError
# 4. Where is the error in the code?
# => While you can multiply a string 5 times, you can't multiply a number by "String" times.
# 5. Why did the interpreter give you this error?
# => The interpreter reads this as "repeat 5 [string] times", and is confused how to make sense of that.

# --- error -------------------------------------------------------

# amount_of_kfc_left = 20/0


# 1. What is the line number where the error occurs?
# => 141
# 2. What is the type of error message?
# =>  divided by 0
# 3. What additional information does the interpreter provide about this type of error?
# =>  ZeroDivisionError
# 4. Where is the error in the code?
# => The assigned value of the variable amound_of_kfc_left is the result of 20/0
# 5. Why did the interpreter give you this error?
# => It's mathematically impossible to divide an integer or floating point number by zero.

# --- error -------------------------------------------------------

# require_relative "cartmans_essay.md"

# 1. What is the line number where the error occurs?
# => 157
# 2. What is the type of error message?
# =>
# 3. What additional information does the interpreter provide about this type of error?
# => `require_relative': cannot load such file -- /Users/jasonallen/Desktop/DBC/phase-0/week-4/cartmans_essay.md
# 4. Where is the error in the code?
# => The relative file called for doesn't exist.
# 5. Why did the interpreter give you this error?
# => The interpreter can't find the file.



# --- REFLECTION -------------------------------------------------------
# # Write your reflection below as a comment.
# Which error was the most difficult to read?
# The error most difficult to read was the error on line 13. It required reading through the whole document to determine what's wrong.

# How did you figure out what the issue with the error was?
# It was an obvious syntax error when I reached the end of the document.

# Were you able to determine why each error message happened based on the code?
# Yes.

# When you encounter errors in your future code, what process will you follow to help you debug?
# Pay attention to the interpreter - it'll tell you exactly what is wrong and where to find it!
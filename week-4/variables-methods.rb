puts "What is your first name?"
first_name = gets.chomp
puts "What is your middle name?"
middle_name = gets.chomp
puts "What is your last name?"
last_name = gets.chomp

puts "That's a long name, " + first_name.capitalize + " " + middle_name.capitalize + " " + last_name.capitalize + "! It's nice to meet you."

puts "What is your favorite guitar amplifier volume, " + first_name.capitalize + "?"
fav_num = gets.chomp
fav_num = fav_num.to_i + 1
puts "That's a great number, " + first_name.capitalize + ", but wouldn't " + fav_num.to_s + " be even better? It's one louder."

# LINK TO OTHER EXERCISES:
# [Address formatting](/address/my_solution.rb)
# Address excercise on github if relative path does not work:
# https://github.com/jasonallenphotography/phase-0/blob/master/week-4/address/my_solution.rb

# [Math](/math/my_solution.rb)
# Math excercise on github if relative path does not work:
# https://github.com/jasonallenphotography/phase-0/blob/master/week-4/math/my_solution.rb

# REFLECTION
# How do you define a local variable?
# A local variable defines a piece of data within a method or other local scope.

# How do you define a method?
# A method is a defined function that can take parameters, and establishes a local scope. It can perform a task using local variables.

# What is the difference between a local variable and a method?
#  A method performs a task or calculation, and may contain local vairables.

# How do you run a ruby program from the command line?
# `$ ruby PROGRAM_NAME.rb` runs a ruby program.

# How do you run an RSpec file from the command line?
# `$ rspec SPECIFICATION_FILE.rb` runs an RSpec in the current directory.

# What was confusing about this material? What made sense?
# The only confusion I had was that the `rspec` in one of the exercises called for `return` instead of `puts`, and was throwing errors at me even though the output was identical to what it expected! I solved this issue by reading the `rspec`, but otherwise I had no confusion in this exercise.
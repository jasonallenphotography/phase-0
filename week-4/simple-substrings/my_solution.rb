# Simple Substrings

# I worked on this challenge with: Julia.


# Your Solution Below
def welcome(address)
  if address.upcase.include? " CA "
    p "Welcome to California"
  else
    p "You should move to California"
  end
end


welcome("946 Bushwick Avenue, Brooklyn NY 11232")
welcome("14 Judith St, Plainview NY 11386")
welcome("5 west 19th st, New York ny 11001")
welcome("123 anystreet San francisco CA 70000")
welcome("12 main street, san diego ca 70000")

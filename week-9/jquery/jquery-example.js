// U3.W9:JQuery


// I worked on this challenge [with: Jason Allen].
// This challenge took me [#] hours.

$(document).ready(function(){

//RELEASE 0:
  //link the image

//RELEASE 1:

  //Link this script and the jQuery library to the jQuery_example.html file and analyze what this code does.

$('body').css({'background-color': 'lightGray'})

//RELEASE 2:
  //Add code here to select elements of the DOM
bodyElement = $('body')

//RELEASE 3:
  // Add code here to modify the css and html of DOM elements
  $('#intro').css({
    'color': 'blue',
    'border': '3px solid black',
    'visibility': 'visible',
    'padding': '1em'
  });





//RELEASE 4: Event Listener
  // Add the code for the event listener here

  $('img').on('mouseover', function(e){
    e.preventDefault()
    $(this).attr('src', 'rock-dove.jpg')
  })

  $('img').on('mouseout', function(f){
    f.preventDefault()
    $(this).attr('src', 'dbc_logo.png')
  })


//RELEASE 5: Experiment on your own
$('button').css({ padding: '2em', margin: '1em'})
$('#b1').on('click', function(e){
  e.preventDefault()
  $('.mascot img').attr('src', 'rock-dove.jpg')
})
$('#b2').on('click', function(e){
  e.preventDefault()
  $('.mascot img').attr('src', 'dbc_logo.png')
})


$('.mascot img').on('click', function(){
  // $(this).animate({ 'height': '+=100px'}, "slow");
  $(this).css({ 'border-style': 'solid'}, 0);
  $(this).css({ 'border-color': 'red'}, 0);
  $(this).animate(
      {'border-width': '+=10px',
      padding: '+=100'},
      1000);
})

})  // end of the document.ready function: do not remove or write DOM manipulation below this.


// What is jQuery?

// This

// What does jQuery do for you?

// This

// What did you learn about the DOM while working on this challenge?

// This
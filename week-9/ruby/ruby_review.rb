# OO Basics: Student


# I worked on this challenge with: Kevin Perkins.
# This challenge took me [4] hours.


# Pseudocode
=begin
  Create 5 student objects with a first_name and five test scores
  Define a method Average that calculates a Student's average from their test scores and assigns it to each student
  Define a method linear_search that returns the student's position in the array, or -1 if not found
  Define a method binary_search that searches the student array for a Student's first_name and returns 0 if found, -1 if not found

=end


# Initial Solution

class Student
  attr_accessor :scores, :first_name, :average, :letter_grade

  def initialize(scores, first_name)
    @first_name = first_name
    @scores = scores
    @average = calc_average(@scores)
    @letter_grade = calc_letter_grade(@average)
  end

end

def calc_average(array)
  sum = 0
  array.each { |x| sum += x }
  sum/array.length
end

def calc_letter_grade(gpa)
  if gpa >= 90
    "A"
  elsif gpa >= 80
    "B"
  elsif gpa >= 70
    "C"
  elsif gpa >= 60
    "D"
  else
    "F"
  end
end

def linear_search(array, query)
  array.index { |x| x.first_name == query } ? 0 : -1
end


# def binary_search(array, query, i_min, i_max)
#   # array = array.sort_by!{|x| x.first_name}
#   #find median of array
#   if array.length.odd?
#     i_mid = array[(array.length - 1)/2]
#   else array.length.even?
#     i_mid = array[array.length/2] + array[array.length/2 - 1]
#   end

#   #3-way comparison
#     if query == array[i_mid]
#       return array[i_mid]
#     elsif query > array[i_mid]
#       binary_search(array, query, i_mid + 1, i_max)
#     else
#       binary_search(array, query, i_min, i_mid - 1)
#     end

# end

students = [
  alex = Student.new([100,100,100,0,100], "Alex"),
  bob = Student.new([90,85,80,85,90], "Bob"),
  carrie = Student.new([95,90,85,90,95], "Carrie"),
  david = Student.new([85,80,75,80,85], "David"),
  elizabeth = Student.new([100,95,95,95,100], "Elizabeth")
]




# Refactored Solution

def binary_search(array, query)
  array.bsearch { |x| query <=> x.first_name } ? 0 : -1
end

# DRIVER TESTS GO BELOW THIS LINE
# Initial Tests:

p students[0].first_name == "Alex"
p students[0].scores.length == 5
p students[0].scores[0] == students[0].scores[4]
p students[0].scores[3] == 0


# Additional Tests 1:

p students[0].average == 80
p students[0].letter_grade == 'B'

# Additional Tests 2:

p linear_search(students, "Alex") == 0
p linear_search(students, "NOT A STUDENT") == -1

# p binary_search(students, "Alex", 0, 4)
# a = [1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15]
# binary_search(a, 4)

p binary_search(students, "Alex") == 0
p binary_search(students, "NOT A STUDENT") == -1

# Reflection
=begin
What concepts did you review in this challenge?
  I reviewed translating theory-based algorithms into Ruby
  code that passes specification tests.

What is still confusing to you about Ruby?
  Calling instance object properties from an array within a
  complex method is still tricky for me, though I still was
  able to make this work with my pair!

What are you going to study to get more prepared for Phase 1?
  I am reading Eloquent Ruby, as well as Practical Object
  Oriented Design in Ruby in preparation for Phase 1.

=end
# Tracking Changes Reflection

## How does tracking and adding changes make developers' lives easier?
Tracking allows developers to quickly identify where an error was introduced into a complex project, and to step back to a last-known working version. Adding changes with commentary allows collaborators to quickly understand the steps another developer is taking.

## What is a commit?
A commit is a snapshot of the current state of a file tracked by git.

## What are the best practices for commit messages?
Commit messages are to be capitalized, in the imperative verb tense (to best match machine-generated comments). They should be 50 characters or less if brief, or as verbose as possible to break down complicated steps.

## What does the HEAD^ argument mean?
The HEAD^ argument refers to the last commit.

## What are the 3 stages of a git change and how do you move a file from one stage to the other?
A git change can be modified, staged, committed. A tracked file becomes modified once edited. 'git add FILENAME' stages the file. 'git commit -m "Description"' or 'git commit -v' and adding notes to the file commits the file and stages a snapshot as the file is.

## Write a handy cheatsheet of the commands you need to commit your changes?
git commit -m "Description" leaves a brief description and commits a change
git commit -v and adding notes within the file commits a change with verbose notes
git push sends the commited files and branch to github

## What is a pull request and how do you create and merge one?
A pull request is a request to merge your changes to a branch into the master.

## Why are pull requests preferred when working with teams?
Pull requests give another developer the opportunity to review and merge your code as a safety check.
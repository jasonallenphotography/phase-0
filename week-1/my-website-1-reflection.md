# My Website - Reflection

## Paste a link to your [USERNAME].github.io repository.
https://github.com/jasonallenphotography/jasonallenphotography.github.io

## Explain how to create a repository on GitHub and clone the repository to your local computer to a non-technical person.
The following steps are how I would go about creating a GitHub repository and cloning a repository to my local machine:
- Navigate to GitHub and log in
- Click the "+" icon on the top right and select 'Add new repository' from the options
- Name and describe your repo, set your privacy, and choose whether to initialize your repository with a readme
- Click `Create Repository`
- Open Terminal
- Navigate to the path where you'd like your local repository to exist
- Enter `git init` to initialize a Git repository at that location
- In your browser, navigate to your new repository. Copy its URL and append `.git` at the end. Copy this to your clipboard.
- Enter `git clone RepoURL.git` to clone the repository to your system at the local path.

## Describe what open source means.
Open source software is software whose source code is availble to the community. It is generally free and community developed, though there may be commercial uses of the compiled code. Users and community members are encoured to build upon the initial version of the software and add additional functionality.

## What do you think about Open Source? Does it make you nervous or protective? Does it feel like utopia?
In my opinion, it really depends on the use case. If you provide a service associated with an open source platform (i.e. hosting wordpress sites) and are comfortable with a community developing the core product for you, it makes a lot of sense, and takes the heavy lifting off of your shoulders! For other commercial use cases, closed source makes a lot more sense (such as proprietary video editing software from Avid, which is the only niche tool for working with certain filetypes). Open source is certainly a utopian ideal, but also utilitarian - let people build what they need on their own if you don't wish to provide it to them as a service!

## Assess the importance of using licenses.
As with many other fields, licensing one's work is important. It's your opportunity to stake your claim to the work you've done, define how you wish for it to be used (or not used), and to share what you've created in a way that it can be built upon or best used by your audience.

## What concepts were solidified in the challenge? Did you have any "aha" moments? What did you struggle with?
Repetion of git workflow certainly helped cement the mechanics of branch checkouts, commits, pushes, and pulls. I hit a minor hiccup when git prompted me to login to my github account on my push command - I'd been using autocomplete on my browser to remember my password... oops! I still feel as though I'm riding a bike with training wheels when it comes to setting up local repositories via terminal and getting them to communicate with my remote GitHub repository on 'git push'... that is, I get the mechanics of it, but I'm not 100% confident with the steps without referring to a reference guide or cheatsheet, and I keep having to `git push origin master` after cloning, or `git remote add origin URLonGitHub.git` to establish communication. I've been creating, cloning to local, commiting, pushing, and merging on additional repositories to practice the mechanics. It's slowly getting more comfortable.

## Did you find any resources on your own that helped you better understand a topic? If so, please list it.
I referred back to prior lessons for the steps I had second thoughts about. The git starter guide on Atlassian, suggested by a classmate, was also helpful as a quick reference.
[Atlassian Git Guide](https://www.atlassian.com/git/tutorials/)

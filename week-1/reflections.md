# Reflections
## Think About Time
I spent most of my time in the allotted hour of research focusing on time boxing, more specifically the Pomodoro technique, and the Fogg Method. As I have an educational background in Industrial/Organizational Psychology, I'm already familiar with the psychology behind motivation and the benefits of starting small.

Time boxing resonated with me - in my own learning style, I often get learning fatigue when trying to absorb a lot of broad content quickly. Currently, I always broken down longer and larger self-taught lessons into the more detailed elements as goals to complete, but I'd never considered fixing it to a time limit or schedule.

Certainly with the volume of content we are preparing to embark upon, building in breaks at intervals is a sound strategy that I plan to implement in my own learning. I've downloaded a kitchen timer app for my phone and already set up a reusable 25 minute countdown timer widget on my phone's home screen.

The Fogg Method of setting a parsimonious goal, making it easy, and anchoring it to a recurring trigger is also known as classical/Pavlovian conditioning in psychology. It has a demonstrated history of being useful in learning, and repetition is the demonstrated key to avoiding extinction (i.e. no longer following through on the conditioned response). I will be using this also during Phase 0.

Currently, I use Google Calendar to manage my time, as it allows for reminder emails and popups to be set, and it allows for scheduling windows of time to complete task expectations for DBC and my freelance work responsibilities. For Phase 0, I plan to keep using what has worked for me professionally for the past 10 years, in addition to the Pomodoro technique of time blocking.

## The Command Line
A shell is the command line interface for digging into your computer's operating system. Bash is the interpreter for Unix, Linux, and OSX operating systems.

Having used Linux previously, and troubleshooting many networking and hardware/software issues on OSX in a video production environment, I was familiar with most of this material, but pipes and redirects were new to me. I didn't have difficulty per se, but I slowed down to pay more careful attention to the new material, and was able to complete all of the exercises without difficulty.

I find that the most useful and important arguments are the navigation functions (pwd, ls, cd, .., pushd/popd etc) as you can't really use any of the file creation or data search arguments without first knowing where you are on the system, or how to get where you're going.

Here are my definitions for the following:
* pwd: prints the working directory, aka where you are on the system
* ls: lists the files in the working directory.
* mv: renames files
* cd: change directory
* ../: when used with cd, brings you up a level in the file structure.
* touch: creates a new file in the working directory with //touch FILENAME.EXTENSION
* mkdir: makes a new directory in the working directory //mkdir NEWDIRECTORYNAME
* less: a pager/text reader function that allows forward and back movement ("space" forward, "b" back)
* rmdir: removes a directory in the working directory
* rm: removes a file in the working directory
* help: displays a list of bash commands
* man: displays the manual for an argument, i.e. //man grep

## Forking and Cloning

My instructions for creating a repository on GitHub would be:
-log into GitHub
-click the "+" icon on the top right (near your user profile icon) and select "New Repository"
-assign an owner from the drop-down menu, and name the new repository
-add a description
-select privacy for the new repository
-click "Create Repository"

For forking a repo on GitHub:
-log into GitHub
-navigate to the repo you wish to fork
-select the "Fork" button on the top right

For cloning a repo on Git via terminal:
-navigate to the repo you wish to clone on GitHub
-select the clone URL and copy it to your clipboard
-open terminal and navigate to your Git master repository
-execute the command //git clone CLONE_URL

Forking a repository is much quicker and easier to create an instance of a repository's files that you own at the push of a button. This is done instead of creating a new repository, as it carries over privacy and license settings, and compares itself to the original repository.